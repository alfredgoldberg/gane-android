ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Risky Project Location:
-----------------------
The tools *should* handle project locations in any directory. However,
due to bugs, placing projects in directories containing spaces in the
path, or characters like ", ' and &, have had issues. We're working to
eliminate these bugs, but to save yourself headaches you may want to
move your project to a location where this is not a problem.
C:\Users\Absolute\Desktop\gane_android (1)1
                                      -    

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .idea\
* .idea\.name
* .idea\compiler.xml
* .idea\copyright\
* .idea\copyright\profiles_settings.xml
* .idea\encodings.xml
* .idea\gane_android (1).iml
* .idea\libraries\
* .idea\libraries\aws_android_sdk_2_1_7_sns.xml
* .idea\misc.xml
* .idea\modules.xml
* .idea\workspace.xml
* README.md
* add-ons\
* add-ons\addon-google_apis-google-17-1\
* add-ons\addon-google_apis-google-17-1\docs\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\allclasses-frame.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\allclasses-noframe.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\effects\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\effects\EffectList.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\effects\package-frame.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\effects\package-summary.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\com\google\android\media\effects\package-tree.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\constant-values.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\deprecated-list.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\help-doc.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\index-all.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\index.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\overview-tree.html
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\package-list
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\resources\
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\resources\inherit.gif
* add-ons\addon-google_apis-google-17-1\docs\effects_apis\stylesheet.css
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\allclasses-frame.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\allclasses-noframe.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\GeoPoint.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\ItemizedOverlay.OnFocusChangeListener.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\ItemizedOverlay.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MapActivity.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MapController.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MapView.LayoutParams.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MapView.ReticleDrawMode.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MapView.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\MyLocationOverlay.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\Overlay.Snappable.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\Overlay.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\OverlayItem.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\Projection.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\TrackballGestureDetector.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\package-frame.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\package-summary.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\com\google\android\maps\package-tree.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\constant-values.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\deprecated-list.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\help-doc.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\index-all.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\index.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\overview-tree.html
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\package-list
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\resources\
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\resources\inherit.gif
* add-ons\addon-google_apis-google-17-1\docs\maps_apis\stylesheet.css
* add-ons\addon-google_apis-google-17-1\images\
* add-ons\addon-google_apis-google-17-1\images\armeabi-v7a\
* add-ons\addon-google_apis-google-17-1\images\armeabi-v7a\NOTICE.txt
* add-ons\addon-google_apis-google-17-1\images\armeabi-v7a\build.prop
* add-ons\addon-google_apis-google-17-1\images\armeabi-v7a\ramdisk.img
* add-ons\addon-google_apis-google-17-1\images\armeabi-v7a\system.img
* build.properties
* ic_launcher-web.png
* pom.xml
* proguard-project.txt

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app\src\main\AndroidManifest.xml
* assets\ => app\src\main\assets\
* libs\aws-android-sdk-2.1.7-sns.jar => app\libs\aws-android-sdk-2.1.7-sns.jar
* libs\urbanairship-lib-4.0.2.jar => app\libs\urbanairship-lib-4.0.2.jar
* lint.xml => app\lint.xml
* res\ => app\src\main\res\
* src\ => app\src\main\java\

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
