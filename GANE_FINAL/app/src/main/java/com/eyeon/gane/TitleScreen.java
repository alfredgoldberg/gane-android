package com.eyeon.gane;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.GCMCommonUtils;
import com.eyeon.gane.utils.GCMRegistrationIntentService;
import com.eyeon.gane.utils.JsonParser;
import com.eyeon.gane.utils.MyLocationListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

//@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("NewApi")
public class TitleScreen extends AppCompatActivity implements OnClickListener {

    // GCM STATIC VARIABLES
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    GoogleCloudMessaging gcm;
    String SENDER_ID = "494205238363";
    String regid;

    // Activity Variables
    //Bundle savedData = new Bundle();
    String sScreen = "";
    String sDeviceID = "";
    String sRegId = "";
    boolean bIsFullVersion = false;
    boolean bIsOnlineMode = false;
    Context mContext;

    // Layout Widgets
    ImageView ivLogo;
    Button bAgency;
    Button bScreenings;
    Button bPlacement;
    Button bLaws;
    Button bReportMissingAdult;
    Button bContact;

    // Activation Widget
    Spinner sprUserType = null;

    // Offline Mode Widgets
    Button bRetryConnection;
    TextView tvAPSNumber;
    TextView tvTERFNumber;
    TextView tvMattiesCallNumber;
    TextView tvSafeReturnNumber;

    // Graphics
    Drawable buttonTouch;
    Drawable buttonNoTouch;

    // Webservices
    Webservices web;
    JsonParser parser;

    // Push Notifications
    ArrayList<String[]> notifications;
    int notificationCounter = 0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Copy Bundle Data
        //savedData = savedInstanceState;
        super.onCreate(savedInstanceState);

        // Get this Activity
        mContext = this;

        // Check the Data connection to determine if Offline Mode is needed
        bIsOnlineMode = isNetworkAvailable();

        //////////////////////////////// NEW CODE ENTERED HERE
        if (bIsOnlineMode) {
            // Check device for Play Services APK.
            if (checkPlayServices()) {
                // If this check succeeds, proceed with normal processing.
                // Otherwise, prompt user to get valid Play Services APK.
                startRegistrationService(true, false);
            }
        }

        // Open a Connection to the DB and get the User Access Code
        DatabaseHandler db = new DatabaseHandler(this);
        String accessCode = db.getAccessCode();
        sDeviceID = db.getDeviceID();
        db.close();

        // Check the length of the Access code, empty string means app is not activated
        if (accessCode.length() == 0)
            bIsFullVersion = false;
        else {
            bIsFullVersion = true;
            invalidateOptionsMenu(); // This is here to the recall onCreateOptionsMenu
        }

        // Get the Button NoTouch/Touch Graphics
        buttonTouch = getResources().getDrawable(R.drawable.button_touch);
        buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

        // Check GPS Permissions
        checkGPSPermissions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Check for Internet Connection and then check if Free Version
        if (bIsOnlineMode) {
            if (!bIsFullVersion) {
                MenuInflater inflater = getMenuInflater();
                inflater.inflate(R.menu.activation, menu);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Create Dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        // Set title and message
        alert.setTitle("Activate Personnel Version");
        alert.setMessage("Enter Activation Code");

        // String Array of the User Types
        String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};

        // Create LinearLayout
        LinearLayout activationLayout = new LinearLayout(this);
        activationLayout.setOrientation(LinearLayout.VERTICAL);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        sprUserType = new Spinner(this);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sprUserType.setAdapter(spinnerArrayAdapter);

        // Add Widgets to Layout
        activationLayout.addView(input);
        activationLayout.addView(sprUserType);

        // Set the Dialog's View
        alert.setView(activationLayout); //input);

        // Initialize the Webservice
        final Webservices activationCall = new Webservices(this);
        DatabaseHandler dbHandler = new DatabaseHandler(this);
        final String regId = dbHandler.getRegId();
        dbHandler.close();

        // Set positive button - Listener is Null, overriden below
        alert.setPositiveButton("Ok", null);

        // Set negative button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Create the Alert Dialog
        final AlertDialog activationDialog = alert.create();

        // Set the Dialog Show Listener to enforce Validation on the User Role Selection
        activationDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                // Get the Positive Button
                Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);

                // Add a Click Listener
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        // Get the value
                        Editable value = input.getText();
                        String userRole = sprUserType.getSelectedItem().toString();

                        if (!userRole.contains("Select")) {
                            // Create the Array and add the values to the array
                            String[] data = new String[3];
                            data[0] = value.toString();    // Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)

                            // Get the GCM Registration ID
                            data[1] = regId; // GCM Registration ID

                            // Get the UserType
                            data[2] = userRole;

                            // Get the results
                            JSONObject results = activationCall.postActivationCode(data);

                            // Parse the Data and return an ArrayList of Questions
                            JsonParser parser = new JsonParser();
                            String[] activationResults = parser.checkPostActivationCodeSubmission(results);

                            if (activationResults.length == 1) {
                                // Activate the App
                                activateApp(data[0]);
                            } else {
                                // Activation Failed - Display Fail Message
                                displayFailedActivationMessage(activationResults[1]);
                            }

                            // Close the Dialog
                            activationDialog.dismiss();
                        } else {
                            // Display Invalid Data Notification
                            Toast.makeText(TitleScreen.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
        });

        // Show Dialog
        activationDialog.show();

        // Return True
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // UrbanAirship
        //if (bIsOnlineMode && UAirship.shared().isFlying())
        //	UAirship.shared().getAnalytics().activityStarted(this);
    }

    @Override
    protected void onResume() {

        //if (savedData != null)
        //	onRestoreInstanceState(savedData);
        bIsOnlineMode = isNetworkAvailable();

        if (bIsOnlineMode) {
            // Instantiate Webservices and JsonParser
            web = new Webservices(this);
            parser = new JsonParser();

            // Create new ArrayList and get UA Apid
            notifications = new ArrayList<String[]>();
            //sRegId = PushManager.shared().getAPID();

            // Get the JSON and parse it
            JSONObject results = web.getUnreadPushNotifications(sRegId);
            notifications = parser.getUnreadNotificationsFromJson(results);

            // Check for Unread Push Notifications
            checkForUnreadPushNotifications();

            ////////////////////////////////NEW CODE ENTERED HERE
            // Check device for Play Services APK.
            checkPlayServices();
        }
        // Load Resources
        loadResources();

        // Call Super Resume
        super.onResume();
    }

    @Override
    protected void onPause() {
        //if (savedData != null)
        //	onSaveInstanceState(savedData);

        super.onPause();
    }

    @Override
    protected void onStop() {
        //if (savedData != null)
        //	onSaveInstanceState(savedData);

        // UrbanAirship
        //if (bIsOnlineMode && UAirship.shared().isFlying())
        //	UAirship.shared().getAnalytics().activityStopped(this);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("screen", sScreen);
        savedInstanceState.putBoolean("version", bIsFullVersion);

        // Call Super Class
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        sScreen = savedInstanceState.getString("screen");
        bIsFullVersion = savedInstanceState.getBoolean("version");

        // Call Super Class
        super.onRestoreInstanceState(savedInstanceState);
    }

    // Load the Activity Resources
    //@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    //@SuppressLint("NewApi")
    private void loadResources() {

        // Check if there is a Data Connection
        if (!bIsOnlineMode) {
            // OFFLINE MODE
            setContentView(R.layout.titlescreen_offline);
            setTitle("Offline Mode");

            // Get Layout Widgets
            bRetryConnection = (Button) findViewById(R.id.btnRetryConnection);
            tvAPSNumber = (TextView) findViewById(R.id.tvAPSPhoneNumber);
            TextView tvTERFLabelSpacer = (TextView) findViewById(R.id.tvTERFTitleSpacer);
            TextView tvTERFLabel = (TextView) findViewById(R.id.tvTERFTitle);
            tvTERFNumber = (TextView) findViewById(R.id.tvTERFPhoneNumber);
            TextView tvMattiesCallLabelSpacer = (TextView) findViewById(R.id.tvMattiesCallTitleSpacer);
            TextView tvMattiesCallLabel = (TextView) findViewById(R.id.tvMattiesCallTitle);
            tvMattiesCallNumber = (TextView) findViewById(R.id.tvMattiesCallPhoneNumber);
            tvSafeReturnNumber = (TextView) findViewById(R.id.tvSafeReturnCallPhoneNumber);

            // Setup ClickListeners
            bRetryConnection.setOnClickListener(this);
            tvAPSNumber.setOnClickListener(this);
            tvTERFNumber.setOnClickListener(this);
            tvMattiesCallNumber.setOnClickListener(this);
            tvSafeReturnNumber.setOnClickListener(this);

            bRetryConnection.setBackgroundResource(R.drawable.laws_button);

            SharedPreferences prefs = getSharedPreferences(Main.class.getSimpleName(), Context.MODE_PRIVATE);

            // Get Numbers from the DB
            tvAPSNumber.setText(prefs.getString("APS", ""));
            tvTERFNumber.setText(prefs.getString("TERF", ""));
            tvMattiesCallNumber.setText(prefs.getString("MATTIE", ""));
            tvSafeReturnNumber.setText(prefs.getString("SAFERETURN", ""));

            // Hide Matties Call and TERF in Free Mode

            DatabaseHandler db = new DatabaseHandler(this);
            String accessCode = db.getAccessCode();
            if (accessCode.length() == 0)
                bIsFullVersion = false;
            else {
                bIsFullVersion = true;
            }

            if (!bIsFullVersion) {
                // Hide TERF Contact Info and remove listener
                tvTERFLabelSpacer.setVisibility(View.GONE);
                tvTERFLabel.setVisibility(View.GONE);
                tvTERFNumber.setVisibility(View.GONE);
                tvTERFNumber.setOnClickListener(null);

                // Hide Matties Call Info and remove listener
                tvMattiesCallLabelSpacer.setVisibility(View.GONE);
                tvMattiesCallLabel.setVisibility(View.GONE);
                tvMattiesCallNumber.setVisibility(View.GONE);
                tvMattiesCallNumber.setOnClickListener(null);
            }
        } else {
            if (bIsFullVersion) {

                // Set the Title
                setContentView(R.layout.titlescreen_full);

                // Set the Title depending on access to the DeviceID
                if (sDeviceID.length() == 0) {
                    setTitle("GANE");
                } else {
                    setTitle("GANE - ID(" + sDeviceID + ")");
                }
                Webservices wbs = new Webservices(mContext);
                JSONObject jsonResult = null;
                try {
                    DatabaseHandler db = new DatabaseHandler(this);

                    String token = db.getRegId();
                    if (!TextUtils.isEmpty(token))
                        jsonResult = wbs.postLogInInfo(token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("LOG IN SERVICE", jsonResult.toString());
                // Create Buttons
                ivLogo = (ImageView)findViewById(R.id.logo);
                bAgency = (Button) findViewById(R.id.btnAgencyFull);
                bScreenings = (Button) findViewById(R.id.btnScreeningsFull);
                bPlacement = (Button) findViewById(R.id.btnPlacementFull);
                bLaws = (Button) findViewById(R.id.btnLawsFull);
                bReportMissingAdult = (Button) findViewById(R.id.btnReportMissingAdultFull);
                bContact = (Button) findViewById(R.id.btnContactFull);

                // Create Button States for each button
                bAgency.setBackgroundResource(R.drawable.laws_button);
                bScreenings.setBackgroundResource(R.drawable.laws_button);
                bPlacement.setBackgroundResource(R.drawable.laws_button);
                bLaws.setBackgroundResource(R.drawable.laws_button);
                bReportMissingAdult.setBackgroundResource(R.drawable.laws_button);
                bContact.setBackgroundResource(R.drawable.laws_button);

                // Set Clicklisteners
                bAgency.setOnClickListener(this);
                bScreenings.setOnClickListener(this);
                bPlacement.setOnClickListener(this);
                bLaws.setOnClickListener(this);
                bReportMissingAdult.setOnClickListener(this);
                bContact.setOnClickListener(this);
            } else if (!bIsFullVersion) {

                // Set the Title
                setContentView(R.layout.titlescreen_free);

                // Set the Title depending on access to the DeviceID
                if (sDeviceID.length() == 0) {
                    setTitle("GANE");
                } else {
                    setTitle("GANE - ID(" + sDeviceID + ")");
                }

                // Create the Buttons
                ivLogo = (ImageView) findViewById(R.id.logo);
                bAgency = (Button) findViewById(R.id.btnAgencyFree);
                bScreenings = (Button) findViewById(R.id.btnScreeningFree);
                bLaws = (Button) findViewById(R.id.btnLawsFree);
                bPlacement = (Button) findViewById(R.id.btnPlacementFree);
                bReportMissingAdult = (Button) findViewById(R.id.btnReportMissingAdultFree);

                // Create Button States for each button
                bAgency.setBackground(getResources().getDrawable(R.drawable.laws_button));
                bScreenings.setBackground(getResources().getDrawable(R.drawable.laws_button));
                bLaws.setBackground(getResources().getDrawable(R.drawable.laws_button));
                bPlacement.setBackground(getResources().getDrawable(R.drawable.laws_button));
                bReportMissingAdult.setBackground(getResources().getDrawable(R.drawable.laws_button));

                // Set Clicklisteners
                bAgency.setOnClickListener(this);
                bScreenings.setOnClickListener(this);
                bLaws.setOnClickListener(this);
                bPlacement.setOnClickListener(this);
                bReportMissingAdult.setOnClickListener(this);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            // Offline Mode Links
            case R.id.btnRetryConnection:

                // Check Network Connection, if Available, then reload Resources
                if (isNetworkAvailable()) {
                    bIsOnlineMode = true;
                    loadResources();
                }

                break;
            case R.id.tvAPSPhoneNumber:
                callPhoneNumber(tvAPSNumber.getText().toString());
                break;
            case R.id.tvTERFPhoneNumber:
                callPhoneNumber(tvTERFNumber.getText().toString());
                break;
            case R.id.tvMattiesCallPhoneNumber:
                callPhoneNumber(tvMattiesCallNumber.getText().toString());
                break;
            case R.id.tvSafeReturnCallPhoneNumber:
                callPhoneNumber(tvSafeReturnNumber.getText().toString());
                break;
            // Free Mode Buttons
            case R.id.btnAgencyFree:
                goToAgency();
                break;
            case R.id.btnScreeningFree:
                goToScreenings();
                break;
            case R.id.btnLawsFree:
                goToLaws();
                break;
            case R.id.btnPlacementFree:
                goToPlacement();
                break;
            case R.id.btnReportMissingAdultFree:
                goToReportMissingAdults();
                break;
            // Full Mode Buttons
            case R.id.btnAgencyFull:
                goToAgency();
                break;
            case R.id.btnScreeningsFull:
                goToScreenings();
                break;
            case R.id.btnPlacementFull:
                goToPlacement();
                break;
            case R.id.btnLawsFull:
                goToLaws();
                break;
            case R.id.btnReportMissingAdultFull:
                goToReportMissingAdults();
                break;
            case R.id.btnContactFull:
                contactAPS();
                break;
            default:
                Toast.makeText(this, "Error Choosing Next Activity", Toast.LENGTH_LONG).show();
        }
    }

    // Call Intent to Agency and pass extras
    private void goToAgency() {
        Intent iAgency = new Intent(this, Categories.class);
        iAgency.putExtra("screen", "agency");
        iAgency.putExtra("version", bIsFullVersion);
        startActivity(iAgency);
    }

    // Call Intent to Screenings and pass extras
    private void goToScreenings() {
        Intent iScreenings = new Intent(this, Categories.class);

        if (bIsFullVersion)
            iScreenings.putExtra("screen", "screenings");
        else
            iScreenings.putExtra("screen", "screenings_ane");

        iScreenings.putExtra("version", bIsFullVersion);
        startActivity(iScreenings);
    }

    // Call Intent to Placement and pass extras
    private void goToPlacement() {
        Intent iPlacement = new Intent(this, Placement.class);
        iPlacement.putExtra("version", bIsFullVersion);
        startActivity(iPlacement);
    }

    // Call Intent to Laws and pass extras
    private void goToLaws() {
        Intent iLaws = new Intent(this, Categories.class);
        iLaws.putExtra("screen", "laws");
        iLaws.putExtra("version", bIsFullVersion);
        startActivity(iLaws);
    }

    // Call Intent to Report Missing Adults and pass extras
    private void goToReportMissingAdults() {
        Intent iReportMissingAdults = new Intent(this, ReportMissingAdult.class);
        iReportMissingAdults.putExtra("version", bIsFullVersion);
        startActivity(iReportMissingAdults);
    }

    // Phone call Intent to Adult Protective Services (APS)
    private void contactAPS() {

        // Instantiate Webservice
        final Webservices callAPS = new Webservices(this);

        // Dialog requesting permission to make Phone call
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Call APS");
        alert.setMessage("Do you want to call Adult Protective Services?");

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Phone Number
                String phoneNumber = null;

                // Call the webservice to get the APS phone number
                JSONObject results = callAPS.getPriorityNumbers();

                // Parse the Data and return an ArrayList of Phone Numbers
                JsonParser parser = new JsonParser();
                ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
                alPhoneNumbers = parser.getPriorityNumbersFromJson(results);

                // Loop through the phone numbers
                for (String[] numbers : alPhoneNumbers) {

                    // Search for APS Contact Information
                    if (numbers[1].contains("APS")) {
                        phoneNumber = numbers[2];
                    }
                }

                // Create Intent to call phone number
                Intent phoneCall = new Intent(Intent.ACTION_CALL);
                phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
                phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(phoneCall);
            }
        });

        // Negative Button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    // Phone call Intent to call a Specific number
    private void callPhoneNumber(String number) {

        // Create a final variable for phone number
        final String phoneNumber = number;

        // Dialog requesting permission to make Phone call
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Call Phone Number");
        alert.setMessage("Do you want to call " + number);

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Create Intent to call phone number
                Intent phoneCall = new Intent(Intent.ACTION_CALL);
                phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
                phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(phoneCall);
            }
        });

        // Negative Button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    // Activation is successful. Add the Access code to the DB and activate the App
    final public void activateApp(String accessCode) {

        // Database connection
        final DatabaseHandler db = new DatabaseHandler(this);
        final String access = accessCode;

        // Dialog to display Successful activation
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Activation Success");
        alert.setMessage("App Activated to Personnel Version");

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Insert the Access Code into the DB and call onCreate
                if (db.updateUserAccessCode(access)) {

                    // Activate the App to Full Version
                    bIsFullVersion = true;

                    // Close the connection to the DB
                    db.close();

                    // Create Restart App Intent
                    Intent restartApp = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(getBaseContext().getPackageName());

                    // Add Flag and start Intent
                    restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(restartApp);
                }

                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();

    }

    // Display a dialog showing the message for Failing to activate the App
    final public void displayFailedActivationMessage(String message) {

        // Dialog to display failed activation
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Activation Failed");
        alert.setMessage(message);

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    // Check for Network Data Connection
    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        boolean netStatus = false;

        if (activeNetworkInfo != null)
            if (activeNetworkInfo.isAvailable())
                if (activeNetworkInfo.isConnected())
                    netStatus = true;

        return netStatus;
    }

    private void checkForUnreadPushNotifications() {

        if (sRegId != null) {

            // Loop through the Messages
            if ((notifications.size() - 1) >= notificationCounter) {

                // Get the Notification Values
                final String notificationId = notifications.get(notificationCounter)[0];
                final String message = notifications.get(notificationCounter)[1];

                // Create Dialog
                AlertDialog.Builder alert = new AlertDialog.Builder(this);

                // Set title and message
                alert.setTitle("New Notification");
                alert.setMessage(message);

                // Set positive button - Listener is Null, overriden below
                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Make new Webservices
                        web = new Webservices(mContext);

                        // Mark notification as Read
                        JSONObject results = web.markNotificationRead(sRegId, notificationId);

                        // Check that Notification has been marked
                        if (!parser.checkMarkNotificationReadFromJson(results)) {
                            Log.e("Checking MarkNotificationReadFromJson",
                                    "Failed to mark Notification(" + notificationId + ") as Read");
                        }

                        // Close the Dialog
                        dialog.dismiss();

                        // Increment the Notification Counter
                        notificationCounter++;

                        // Check for more unread Notifications
                        checkForUnreadPushNotifications();
                    }
                });

                // Create the Alert Dialog
                AlertDialog activationDialog = alert.create();

                // Show Dialog
                activationDialog.show();
            }
        }
    }

    private void checkGPSPermissions() {

        // Get GeoCoords
        MyLocationListener listener = new MyLocationListener(this);

        if (!listener.canGetLocation()) {
            // Prompt User for GEO Location Permissions
            listener.showSettingsAlert();
        }
    }


    //////////////////////////////// NEW CODE ENTERED HERE /////////////////////////////////


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("checkPlayServices", "This device is not supported.");
                finish();
            }

            return false;
        }

        return true;
    }

    private String getRegistrationId() {

        // Instantiate DatabaseHandler
        DatabaseHandler db = new DatabaseHandler(mContext);
        String registrationId = db.getRegId();

        if (registrationId == null || registrationId.isEmpty()) {
            Log.i("GCM", "Registration not found.");
            return "";
        }

        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.

        int registeredVersion = db.getAppVersion();
        int currentVersion = getAppVersion(mContext);
        if (registeredVersion != currentVersion) {
            Log.i("GCM", "App version changed.");
            return "";
        }

        return registrationId;
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP
     * or CCS to send messages to your app. Not needed for this demo since the
     * device sends upstream messages to a server that echoes back the message
     * using the 'from' address in the message.
     */

	/*
    private void sendRegistrationIdToBackend()
	{
	    // Initialize Webservice and JsonParser class
		Webservices web = new Webservices(this);
		JsonParser parser = new JsonParser();

		// TODO: Submit GCM Registration ID to Server and check for confirmation


	}
	*/

    /**
     * Stores the registration ID and app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId   registration ID
     */
    /*
    private void storeRegistrationId(String regId)
	{

		// Get current app version and add note to log
		int appVersion = getAppVersion(mContext);
		Log.i("GCM", "Saving regId on app version " + appVersion);

		// Initialize a DatabaseHandler
		DatabaseHandler db = new DatabaseHandler(this);

		// Update the Registration ID and the App Version number
		db.updateUserRegId(regId);
		db.updateAppVersion(appVersion);

		// Close the DB
		db.close();
	}
	*/
    public void startRegistrationService(boolean reg, boolean tkr) {

        if (GCMCommonUtils.checkPlayServices(this)) {
            // Toast.makeText(this, "Background service started...", Toast.LENGTH_LONG).show();
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, GCMRegistrationIntentService.class);
            intent.putExtra("register", reg);
            intent.putExtra("tokenRefreshed", tkr);
            startService(intent);
        }
    }
}