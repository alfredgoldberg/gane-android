package com.eyeon.gane;

import org.json.JSONObject;
import org.json.JSONException;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.util.StateSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

public class Placement extends AppCompatActivity implements OnClickListener
{

	// Activity Variables
	Bundle savedData = new Bundle();
	boolean bIsOnlineMode = false;
	boolean bIsFullVersion = false;
	
	// Layout Widgets
	Button bHousingOptions;
	Button bTERF;
	
	// Graphics
	Drawable buttonTouch;
	Drawable buttonNoTouch;

	Webservices web;

	// Activation Widget
	Spinner sprUserType = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// Get Save state and call super
		savedData = savedInstanceState;
		super.onCreate(savedData);
		
		// Check the Data connection to determine if Offline Mode is needed
		bIsOnlineMode = isNetworkAvailable();
		
		// Set Layout and Title
		setContentView(R.layout.placement);
		setTitle("Placement");
		
		// Get Intent
		bIsFullVersion = getIntent().getExtras().getBoolean("version");
		
		// Get the Button NoTouch/Touch Graphics
		buttonTouch = getResources().getDrawable(R.drawable.button_touch);
		buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);
		
		// Get Housing Options Button
		bHousingOptions = (Button)findViewById(R.id.bHousingOptions);
		
		// Create the clickable Effects
		StateListDrawable emerPlacebuttonEffects = new StateListDrawable();
		emerPlacebuttonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
		emerPlacebuttonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
		
		// Set Button Effect and Click Listener
		bHousingOptions.setBackground(emerPlacebuttonEffects);
		bHousingOptions.setOnClickListener(this);
		
		// Get TERF Button Widget
		bTERF = (Button)findViewById(R.id.bTerf);
		
		// Check if Full Mode
		if (bIsFullVersion)
		{
			// Create Button Effect
			StateListDrawable terfButtonEffects = new StateListDrawable();
			terfButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
			terfButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
			
			// Reset the Graphic
			bTERF.setBackground(terfButtonEffects);
			
			// Set Listener
			bTERF.setOnClickListener(this);
		}
		else
		{
			bTERF.setVisibility(View.INVISIBLE);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		
		// Check for Internet Connection and then check if Free Version
		if (bIsOnlineMode)
		{
			if (!bIsFullVersion)
			{
				MenuInflater inflater = getMenuInflater();
				inflater.inflate(R.menu.activation, menu);
			}
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Create Dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		// Set title and message
		alert.setTitle("Activate Personnel Version");
		alert.setMessage("Enter Activation Code");
		
		// String Array of the User Types
		String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};
		
		// Create LinearLayout
		LinearLayout activationLayout = new LinearLayout(this);
		activationLayout.setOrientation(LinearLayout.VERTICAL);
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		sprUserType = new Spinner(this);

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sprUserType.setAdapter(spinnerArrayAdapter);
		
		// Add Widgets to Layout
		activationLayout.addView(input);
		activationLayout.addView(sprUserType);

		// Set the Dialog's View
		alert.setView(activationLayout); //input);
		
		// Initialize the Webservice
		final Webservices activationCall = new Webservices(this);
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		final String regId = dbHandler.getRegId();
		dbHandler.close();
		
		// Set positive button - Listener is Null, overriden below
		alert.setPositiveButton("Ok", null);

		// Set negative button
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
		  public void onClick(DialogInterface dialog, int whichButton) 
		  {
			  dialog.dismiss();
		  }
		});
		
		// Create the Alert Dialog
		final AlertDialog activationDialog = alert.create();
		
		// Set the Dialog Show Listener to enforce Validation on the User Role Selection
		activationDialog.setOnShowListener(new DialogInterface.OnShowListener() 
		{
			
			@Override
			public void onShow(DialogInterface dialog) 
			{
				
				// Get the Positive Button
				Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				// Add a Click Listener
				positiveButton.setOnClickListener(new View.OnClickListener() 
				{

		            @Override
		            public void onClick(View view) 
		            {
		                // TODO Do something
		            	// Get the value
						Editable value = input.getText();
						String userRole = sprUserType.getSelectedItem().toString();
						
						if (!userRole.contains("Select"))
						{
							// Create the Array and add the values to the array
							String[] data = new String[3];
							data[0] = value.toString(); 	// Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)
							
							// Get the GCM Registration ID
							data[1] = regId; // GCM Registration ID	  
							
							// Get the UserType
							data[2] = userRole;
							
						    // Get the results
							JSONObject results = activationCall.postActivationCode(data);

							// Parse the Data and return an ArrayList of Questions
							JsonParser parser = new JsonParser();
							String[] activationResults = parser.checkPostActivationCodeSubmission(results);
							
							if (activationResults.length == 1)
							{
								// Activate the App
								activateApp(data[0]);
							}
							else
							{
								// Activation Failed - Display Fail Message
								displayFailedActivationMessage(activationResults[1]);
							}
			            	
			                // Close the Dialog
			            	activationDialog.dismiss();
						}
						else
						{
							// Display Invalid Data Notification
							Toast.makeText(Placement.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
						}

		            }
		        });
			}
		});

		// Show Dialog
		activationDialog.show();

		// Return True
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);
		
		// Resume
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public void onClick(View v)
	{

		switch(v.getId())
		{
		case R.id.bHousingOptions:
			
			// Go to Map2Care Website
			Intent iAgency = new Intent(this, URLWebViewActivity.class);
			String url = "http://www.gamap2care.info/Locator.html#cat=LongTermCare&type=Personal+Care+Home";
			iAgency.putExtra("url", url);
			iAgency.putExtra("is_terf", false);
			startActivity(iAgency);
			
			break;

		case R.id.bTerf:
	//Moved to emergency placement
		/*DatabaseHandler db = new DatabaseHandler(this);
			String sDeviceID = db.getRegId();
			web = new Webservices(getApplicationContext());
			try {
				JSONObject jsonObject =	web.postTerf(sDeviceID);
				Log.e("JSON TERF",jsonObject.toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}*/
	//
			Intent iEmergencyPlacement = new Intent(this, EmergencyPlacement.class);
			startActivity(iEmergencyPlacement);

			break;
		default:
		}
		
	}
	
	// Activation is successful. Add the Access code to the DB and activate the App
	final public void activateApp(String accessCode)
	{
		
		// Database connection
		final DatabaseHandler db = new DatabaseHandler(this);
		final String access = accessCode;
		
		// Dialog to display Successful activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Success");
		alert.setMessage("App Activated to Personnel Version");

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				
				// Insert the Access Code into the DB and call onCreate
				if (db.updateUserAccessCode(access))
				{
					
					// Activate the App to Full Version
					bIsFullVersion = true;
					
					// Close the connection to the DB
					db.close();

					// Create Restart App Intent
					Intent restartApp = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(getBaseContext().getPackageName());
					
					// Add Flag and start Intent
					restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(restartApp);
				}
				
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();

	}
	
	// Display a dialog showing the message for Failing to activate the App
	final public void displayFailedActivationMessage(String message)
	{
		
		// Dialog to display failed activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Failed");
		alert.setMessage(message);

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();
	}

	// Check for Network Data Connection
	private boolean isNetworkAvailable() 
	{
		
	    ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
    
	    boolean netStatus = false;

	    if (activeNetworkInfo != null)
	    	if (activeNetworkInfo.isAvailable())
	    		if (activeNetworkInfo.isConnected())
	    			netStatus = true;

	    return netStatus;
	}
}
