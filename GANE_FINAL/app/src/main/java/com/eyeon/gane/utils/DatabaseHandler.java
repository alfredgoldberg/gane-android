package com.eyeon.gane.utils;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

public class DatabaseHandler extends SQLiteOpenHelper
{

    // Database Version
    private static final int DATABASE_VERSION = 2;
 
    // Database Name
    private static final String DATABASE_NAME = "GANEManager.db";
 
    // GANEManager table names
    private static final String TABLE_USER = "user";
    private static final String TABLE_PRIORITY_NUMBERS = "priority_numbers";
 
    // Priority Numbers Table Columns names
    private static final String KEY_PRIORITY_NUMBERS_ID = "id";
    private static final String KEY_PRIORITY_NUMBERS_NAME = "name";
    private static final String KEY_PRIORITY_NUMBERS_PHONE_NO = "phone_number";
    
    // User Table Columns names
    private static final String KEY_USER_ID = "id";
    private static final String KEY_USER_ACCESS_CODE = "access_code";
    private static final String KEY_USER_APID = "apid";
    private static final String KEY_USER_FIRST_OPEN = "first_open";
    private static final String KEY_USER_DEVICE_ID = "user_id";
    private static final String KEY_USER_REGISTRATION_ID = "registration_id";
    private static final String KEY_USER_APP_VERSION = "app_version";
    private Context mContext;
 
    // Constructor
    public DatabaseHandler(Context context) 
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    public DatabaseHandler(Context context, String name)
    {
        super(context, name, null, DATABASE_VERSION);
        mContext = context;
    }
 
    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db)
    {

        //////////// Create the Tables //////////////
		// Write the SQL statement for creating the user DB
        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "("
        		+ KEY_USER_ID + " INTEGER PRIMARY KEY," + KEY_USER_ACCESS_CODE + " TEXT,"
        		+ KEY_USER_APID + " TEXT," + KEY_USER_FIRST_OPEN + " INTEGER,"
        		+ KEY_USER_DEVICE_ID + " TEXT," + KEY_USER_REGISTRATION_ID + " TEXT,"
        		+ KEY_USER_APP_VERSION + " INTEGER)";
        
        // Create user db if not already present and add this user to the table
        //db.execSQL(("DROP TABLE IF EXISTS " + TABLE_USER));
        db.execSQL(CREATE_USER_TABLE);

    	// Write the SQL statement for creating the phone_numbers DB
        String CREATE_PRIORITY_NUMBERS_TABLE = "CREATE TABLE " + TABLE_PRIORITY_NUMBERS + "("
                + KEY_PRIORITY_NUMBERS_ID + " INTEGER PRIMARY KEY," + KEY_PRIORITY_NUMBERS_NAME + " TEXT,"
                + KEY_PRIORITY_NUMBERS_PHONE_NO + " TEXT)";
        
        // Create the phone_number DB if not already present
        db.execSQL(CREATE_PRIORITY_NUMBERS_TABLE);
        
        
        
        //////////// Create the Default User //////////////
        // Create a KeyValue Pairs
    	ContentValues userValues = new ContentValues();

    	// Get the values and put them in the Pair
    	userValues.put(KEY_USER_ID, 1); // User Id Name
    	userValues.put(KEY_USER_ACCESS_CODE, ""); // User Access Code
    	userValues.put(KEY_USER_APID, ""); // UA APID - OLD CODE
    	userValues.put(KEY_USER_FIRST_OPEN, 0); // Flag for first time App is opened
    	userValues.put(KEY_USER_DEVICE_ID, ""); // Device ID
    	userValues.put(KEY_USER_REGISTRATION_ID, ""); // GCM Registration ID
    	userValues.put(KEY_USER_APP_VERSION, 3); // Current App Version

    	// Insert Row
    	db.insert(TABLE_USER, null, userValues);
    }
 
    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
    {
    	/*
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PRIORITY_NUMBERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
 
        // Create tables again
        onCreate(db);
        */
    	
    	Log.d("DatabaseHandler Upgrade", String.format("GANEManager.onUpgrade(%d -> %d)", oldVersion, newVersion));
    	
        switch(newVersion) 
        {
        case 2:
                Log.d("DatabaseHandler Upgrade", "Adding GCM Registration ID and App Version columns to User Table");
                db.execSQL("ALTER TABLE " + TABLE_USER +  " ADD COLUMN " + KEY_USER_REGISTRATION_ID + " TEXT");
                db.execSQL("ALTER TABLE " + TABLE_USER +  " ADD COLUMN " + KEY_USER_APP_VERSION + " INTEGER");
                break;
        default:
                throw new IllegalStateException("onUpgrade() with unknown newVersion" + newVersion);
        }
    }
    
    // Check if the record exists
    public boolean checkIfPriorityNumberExistsByName(String name)
    {
    	
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
    	
        // Create Query string
        String Query = "Select * from " + TABLE_PRIORITY_NUMBERS + " where " + KEY_PRIORITY_NUMBERS_NAME + "=\"" + name + "\"";
        
        // Get Cursor location of Query
        Cursor cursor = db.rawQuery(Query, null);
        
        // Check if Record exists
        if(cursor != null)
        	return true;
        
        // Record does not exist
        return false;
    }
    
    // Add the number to the database
    public void addPriorityNumber(String[] number)
    {
    	// Get the DB
    	SQLiteDatabase db = this.getWritableDatabase();
    	 
    	// Create a KeyValue Pairs
		ContentValues values = new ContentValues();
		
		// Get the values and put them in the Pair
		values.put(KEY_PRIORITY_NUMBERS_ID, number[0]);	// Priority's Id
		values.put(KEY_PRIORITY_NUMBERS_NAME, number[1]); // Priority's Name
        values.put(KEY_PRIORITY_NUMBERS_PHONE_NO, number[2]); // Priority's Phone Number
		
        // Insert Row
        db.insert(TABLE_PRIORITY_NUMBERS, null, values);

    	// Close the connection
        db.close();
    }

    // Add the number to the database
    public void addPriorityNumbers(ArrayList<String[]> numbers)
    {
    	// Get the DB
    	SQLiteDatabase db = this.getWritableDatabase();
    	 
    	// Loop through the Array List
    	for (String[] contact : numbers)
		{
    		// Create a KeyValue Pairs
    		ContentValues values = new ContentValues();
    		
    		// Get the values and put them in the Pair
    		values.put(KEY_PRIORITY_NUMBERS_NAME, contact[1]); 		// Priority's Name
            values.put(KEY_PRIORITY_NUMBERS_PHONE_NO, contact[2]); 	// Priority's Phone Number
    		
            // Insert Row
            db.insert(TABLE_PRIORITY_NUMBERS, null, values);
		}
 
    	// Close the connection
        db.close();
    }
    
    // Updating Phone Number
    public boolean updatePhoneNumber(String[] number) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // Create NameValue pairs
    	ContentValues values = new ContentValues();
    	
    	// Add values to the db
        values.put(KEY_PRIORITY_NUMBERS_NAME, number[1]);
        values.put(KEY_PRIORITY_NUMBERS_PHONE_NO, number[2]);
        
        // Call to update
        try
        {
        	db.update(TABLE_PRIORITY_NUMBERS, values, KEY_PRIORITY_NUMBERS_NAME + " = ?", new String[] { number[1] });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating Priority Numbers fail", "Failed to update " + number[1]);
        	return false;
        }

        // Return True if successful
        return true;
    }
    
    // Updating Phone Numbers
    public boolean updatePhoneNumbers(ArrayList<String[]> numbers) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
        // Loop through numbers array updating all of them
        for (String[] contact : numbers)
        {
        	// Create NameValue pairs
        	ContentValues values = new ContentValues();
        	
        	// Add values to the db
            values.put(KEY_PRIORITY_NUMBERS_NAME, contact[1]);
            values.put(KEY_PRIORITY_NUMBERS_PHONE_NO, contact[2]);
            
            // Call to update
            try
            {
            	db.update(TABLE_PRIORITY_NUMBERS, values, KEY_PRIORITY_NUMBERS_ID + " = ?", new String[] { String.valueOf(contact[0]) });
            }
            catch (Exception e)
            {
            	// Log error and return false
            	Log.e("Updating Priority Numbers fail", "Failed to update " + contact[0] + " " + contact[1]);
            	return false;
            }
        }

        // Return True if successful
        return true;
    }
    
    // Getting single contact
    public String getPhoneNumberByName(String name) 
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
        
        // rawQuery("SELECT id, name FROM people WHERE name = ? AND id = ?", new String[] {"David", "2"});
        
        // Create Query string
        String Query = "Select * from " + TABLE_PRIORITY_NUMBERS + " where " + KEY_PRIORITY_NUMBERS_NAME + "=\"" + name + "\"";
        
        // Number text
        String number = "";
        
        // Get Cursor location of Query
        Cursor cursor = db.rawQuery(Query, null);
        String[] data = null;
        if (cursor.moveToFirst()) 
        {
            do 
            {
            	String test = cursor.toString();
            } while (cursor.moveToNext());
        }
        db.close();
                 
        
        /*
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_PRIORITY_NUMBERS, new String[] 
        		{ KEY_PRIORITY_NUMBERS_PHONE_NO }, KEY_PRIORITY_NUMBERS_NAME + " = ?",
                new String[] { name }, null, null, null, null);

        
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
        	number = cursor.getString(cursor.getColumnIndex(KEY_PRIORITY_NUMBERS_PHONE_NO));
        else
        	Log.e("getPhoneNumberByName Access to DB", "Access to DB Failed");
        	*/
        
        // Close the cursor
        cursor.close();

        // Return the Priority Number
        return number;
    }

    // Add user to user table
    public void addUser(String userId, String userCode, String gcmToken)
    {
    	// Get a writable DB
    	SQLiteDatabase db = this.getWritableDatabase();
    	 
    	// Create a KeyValue Pairs
    	ContentValues values = new ContentValues();

    	// Get the values and put them in the Pair
    	values.put(KEY_USER_ID, userId); // User Id Name
    	values.put(KEY_USER_ACCESS_CODE, "null"); // User Access Code
    	values.put(KEY_USER_REGISTRATION_ID, gcmToken); // GCM Registration ID
    	values.put(KEY_USER_FIRST_OPEN, false); // First Open Flag
    	values.put(KEY_USER_APP_VERSION, 0);

    	// Insert Row
    	db.insert(TABLE_USER, null, values);

    	// Close the connection
    	db.close();
    }

    // Update User Access Code
    public boolean updateUserAccessCode(String access_code) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
    	// Create NameValue pairs
    	ContentValues values = new ContentValues();
    	
    	// Add values to the db
        values.put(KEY_USER_ACCESS_CODE, access_code);
        
        // Call to update
        try
        {
        	db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[] { String.valueOf(1) });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating User Access Code fail", "Failed to update " + access_code);
        	return false;
        }

        // Return True if successful
        return true;
    }
	
    // Getting User Access Code
    public String getAccessCode() 
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
 
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_USER, new String[] 
        		{ KEY_USER_ACCESS_CODE }, KEY_USER_ID + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
        	return cursor.getString(cursor.getColumnIndex(KEY_USER_ACCESS_CODE));
        else
        	Log.e("getAccessCode from DB", "Access to DB Failed");

        // Return the Priority Number
        return "";
    }

    // Update User Registration ID for GCM
    public boolean updateUserRegId(String regId) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
    	// Create NameValue pairs
    	ContentValues values = new ContentValues();
        values.put(KEY_USER_REGISTRATION_ID, regId);
        
        // Call to update
        try
        {
        	db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[] { String.valueOf(1) });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating GCM Registration ID", "Failed to update " + regId);
        	return false;
        }

        // Return True if successful
        return true;
    }
	
    // Getting User Registration Id for GCM
    public String getRegId() 
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
        
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_USER, new String[] 
        		{ KEY_USER_REGISTRATION_ID }, KEY_USER_ID + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        
        // Registration ID String value
        String regId = "";
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
            regId = cursor.getString(cursor.getColumnIndex(KEY_USER_REGISTRATION_ID));

        if(TextUtils.isEmpty(regId)) {
            SharedPreferences sharedPreferences;
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            regId = sharedPreferences.getString(GCMSharedPrefs.REG_ID, "");
        }
        // Return the GCM Registration Id
        return regId;
    }
    
    // Update First Open flag
    public boolean updateFirstOpen(boolean isFirst) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
    	// Create NameValue pairs
    	ContentValues values = new ContentValues();
    	
    	// Add values to the db
        values.put(KEY_USER_FIRST_OPEN, isFirst);
        
        // Call to update
        try
        {
        	db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[] { String.valueOf(1) });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating User First Open", "Failed to update " + isFirst);
        	return false;
        }

        // Return True if successful
        return true;
    }
	
    // Getting First Open Flag
    public boolean isFirstOpen() 
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
 
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_USER, new String[] 
        		{ KEY_USER_FIRST_OPEN }, KEY_USER_ID + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
        {
        	// Get Value
        	int isFirst = cursor.getInt(cursor.getColumnIndex(KEY_USER_FIRST_OPEN));
        	
        	// Check for False
        	if (isFirst == 0)
        		return false;
        }
        else
        {
        	Log.e("isFirstOpen DB Check", "DB Check Fail");
        }

        // First time app is opened
        return true;
    }
    
    // Update Device ID
    public boolean updateDeviceId(String device_id) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
    	// Create NameValue pairs
    	ContentValues values = new ContentValues();
        values.put(KEY_USER_DEVICE_ID, device_id);
        
        // Call to update
        try
        {
        	db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[] { String.valueOf(1) });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating Device ID", "Failed to update " + device_id);
        	return false;
        }

        // Return True if successful
        return true;
    }
    
    // Get Device ID
    public String getDeviceID()
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
        
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_USER, new String[] 
        		{ KEY_USER_DEVICE_ID }, KEY_USER_ID + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        
        // Device ID String value
        String device_id = "";
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
        	device_id = cursor.getString(cursor.getColumnIndex(KEY_USER_DEVICE_ID));

        if(TextUtils.isEmpty(device_id))
        {
           // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
           // device_id = preferences.getString("user_id_first_open", "");
        }
        // Return the Device Id
        return device_id;
    }
    
    //////////////////////// NEW
    
    // Update App Version number
    public boolean updateAppVersion(int version) 
    {
    	// Get Writable Connection to DB
        SQLiteDatabase db = this.getWritableDatabase();
 
    	// Create NameValue pairs
    	ContentValues values = new ContentValues();
        values.put(KEY_USER_APP_VERSION, version);
        
        // Call to update
        try
        {
        	db.update(TABLE_USER, values, KEY_USER_ID + " = ?", new String[] { String.valueOf(1) });
        }
        catch (Exception e)
        {
        	// Log error and return false
        	Log.e("Updating App Version Number", "Failed to update " + version);
        	return false;
        }

        // Return True if successful
        return true;
    }
	
    // Getting Known App Version
    public int getAppVersion() 
    {
    	// Get the Database
        SQLiteDatabase db = this.getReadableDatabase();
        
        // Create Query string and access table
        Cursor cursor = db.query(TABLE_USER, new String[] 
        		{ KEY_USER_APP_VERSION }, KEY_USER_ID + "=?",
                new String[] { String.valueOf(1) }, null, null, null, null);
        
        // App Version
        int appVersion = 0;
        
        // Move Cursor to listing
        if (cursor.moveToFirst())
        	appVersion = cursor.getInt(cursor.getColumnIndex(KEY_USER_APP_VERSION));

        // Return the App Version
        return appVersion;
    }

    // Check if the record exists
    public boolean checkIfUserIdExists(String name)
    {

        // Get the Database
        SQLiteDatabase db = this.getReadableDatabase();

        // Create Query string
        String Query = "Select * from " + TABLE_USER + " where " + KEY_USER_ID + "=\"" + name + "\"";

        // Get Cursor location of Query
        Cursor cursor = db.rawQuery(Query, null);

        if(cursor.getCount() <= 0){
            cursor.close();
            return false;
        }
        cursor.close();
        return true;
    }
}











