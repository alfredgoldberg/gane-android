package com.eyeon.gane;

import java.util.ArrayList;

import org.json.JSONObject;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ListItemDetails extends AppCompatActivity implements OnClickListener
{

	// Activity Variables
	String sScreen = "";
	boolean bIsFullVersion = false;
	String sAgencyId = "";
	Bundle savedData = new Bundle();
	
	// Activation Widget
	Spinner sprUserType = null;
	
	// Agency Variables {Name, Description, Hours, Phone, URL}
	ArrayList<String> alAgencyData;
	TextView tvPhone;
	TextView tvAgencyURL;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// Get Save state and Create
		savedData = savedInstanceState;
		super.onCreate(savedData);
		
		// Get Intent and pull extra to determine which screen to draw
		Intent intent = getIntent();
		sAgencyId = intent.getStringExtra("agency_id");
		bIsFullVersion = intent.getBooleanExtra("version", false);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Only draw in Free Version
		if (!bIsFullVersion)
		{
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.activation, menu);
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Create Dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		// Set title and message
		alert.setTitle("Activate Personnel Version");
		alert.setMessage("Enter Activation Code");
		
		// String Array of the User Types
		String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};
		
		// Create LinearLayout
		LinearLayout activationLayout = new LinearLayout(this);
		activationLayout.setOrientation(LinearLayout.VERTICAL);
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		sprUserType = new Spinner(this);

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sprUserType.setAdapter(spinnerArrayAdapter);
		
		// Add Widgets to Layout
		activationLayout.addView(input);
		activationLayout.addView(sprUserType);

		// Set the Dialog's View
		alert.setView(activationLayout); //input);
		
		// Initialize the Webservice
		final Webservices activationCall = new Webservices(this);
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		final String regId = dbHandler.getRegId();
		dbHandler.close();
		
		// Set positive button - Listener is Null, overriden below
		alert.setPositiveButton("Ok", null);

		// Set negative button
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
		  public void onClick(DialogInterface dialog, int whichButton) 
		  {
			  dialog.dismiss();
		  }
		});
		
		// Create the Alert Dialog
		final AlertDialog activationDialog = alert.create();
		
		// Set the Dialog Show Listener to enforce Validation on the User Role Selection
		activationDialog.setOnShowListener(new DialogInterface.OnShowListener() 
		{
			
			@Override
			public void onShow(DialogInterface dialog) 
			{
				
				// Get the Positive Button
				Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				// Add a Click Listener
				positiveButton.setOnClickListener(new View.OnClickListener() 
				{

		            @Override
		            public void onClick(View view) 
		            {
		                // TODO Do something
		            	// Get the value
						Editable value = input.getText();
						String userRole = sprUserType.getSelectedItem().toString();
						
						if (!userRole.contains("Select"))
						{
							// Create the Array and add the values to the array
							String[] data = new String[3];
							data[0] = value.toString(); 	// Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)
							
							// Get the Registration ID
							data[1] = regId; // GCM Registration ID  
							
							// Get the UserType
							data[2] = userRole;
							
						    // Get the results
							JSONObject results = activationCall.postActivationCode(data);

							// Parse the Data and return an ArrayList of Questions
							JsonParser parser = new JsonParser();
							String[] activationResults = parser.checkPostActivationCodeSubmission(results);
							
							if (activationResults.length == 1)
							{
								// Activate the App
								activateApp(data[0]);
							}
							else
							{
								// Activation Failed - Display Fail Message
								displayFailedActivationMessage(activationResults[1]);
							}
			            	
			                // Close the Dialog
			            	activationDialog.dismiss();
						}
						else
						{
							// Display Invalid Data Notification
							Toast.makeText(ListItemDetails.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
						}

		            }
		        });
			}
		});

		// Show Dialog
		activationDialog.show();

		// Return True
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);
		
		// Initialize the ArrayList
		alAgencyData = new ArrayList<String>();
		
		// Make Webservice Call to get list of Agency Categories
		Webservices Agency = new Webservices(this);
		JSONObject results = Agency.getAgencyById(sAgencyId);
		
		// Parse the Data and return an ArrayList of Questions
		JsonParser parser = new JsonParser();
		alAgencyData = parser.getAgencyByIdFromJson(results);

		// Set the Layout and Title
		setContentView(R.layout.agency_item_details);
		setTitle("Agency Details");
		
		// Set the Text Values
		TextView tvAgencyName = (TextView)findViewById(R.id.tvAgencyName);
		tvAgencyName.setText(alAgencyData.get(1));
		
		EditText etAgencyDetails = (EditText)findViewById(R.id.etAgencyDetails);
		etAgencyDetails.setText(alAgencyData.get(2));
		
		TextView tvHours = (TextView)findViewById(R.id.tvAgencyHours);
		tvHours.setMaxLines(2);
		tvHours.setText(alAgencyData.get(3));
		
		// Phone Number and ClickListener
		tvPhone = (TextView)findViewById(R.id.tvAgencyPhoneNumber);
		tvPhone.setMaxLines(2);
		tvPhone.setText(alAgencyData.get(4));
		tvPhone.setOnClickListener(this);
		
		// URL and ClickListener
		tvAgencyURL = (TextView)findViewById(R.id.tvAgencyWebsiteUrl);
		tvAgencyURL.setText(alAgencyData.get(5));
		tvAgencyURL.setOnClickListener(this);
		
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		
		savedInstanceState.putString("screen", sScreen);
		savedInstanceState.putBoolean("version", bIsFullVersion);
		
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		
		sScreen = savedInstanceState.getString("screen");
		bIsFullVersion = savedInstanceState.getBoolean("version", false);
		
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	
	@Override
	public void onClick(View v) 
	{
		
		switch(v.getId())
		{
		case R.id.tvAgencyPhoneNumber:
			
			// Dialog requesting permission to make Phone call
			AlertDialog.Builder alert = new AlertDialog.Builder(this);
			alert.setTitle("Call Phone Number");
			alert.setMessage("Do you want to call " + tvPhone.getText());

			// Positive Button
			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int whichButton) 
				{
					// Create Intent to call phone number
					Intent phoneCall = new Intent(Intent.ACTION_CALL);
					phoneCall.setData(Uri.parse("tel:" + Uri.encode(tvPhone.getText().toString())));
					phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(phoneCall);
				}
			});

			// Negative Button
			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
			{
			    public void onClick(DialogInterface dialog, int whichButton) 
			    {
			      // Canceled.
				    dialog.dismiss();
			    }
			});

			// Display Dialog
			alert.show();

			break;
		case R.id.tvAgencyWebsiteUrl:
			
			Intent iAgency = new Intent(this, URLWebViewActivity.class);
			iAgency.putExtra("url", tvAgencyURL.getText().toString());
			iAgency.putExtra("is_terf", false);
			startActivity(iAgency);
			
			break;
		}
	}
	
	// Activation is successful. Add the Access code to the DB and activate the App
	final public void activateApp(String accessCode)
	{
		
		// Database connection
		final DatabaseHandler db = new DatabaseHandler(this);
		final String access = accessCode;
		
		// Dialog to display Successful activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Success");
		alert.setMessage("App Activated to Personnel Version");

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				
				// Insert the Access Code into the DB and call onCreate
				if (db.updateUserAccessCode(access))
				{
					
					// Activate the App to Full Version
					bIsFullVersion = true;
					
					// Close the connection to the DB
					db.close();

					// Create Restart App Intent
					Intent restartApp = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(getBaseContext().getPackageName());
					
					// Add Flag and start Intent
					restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(restartApp);
				}
				
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();

	}
	
	// Display a dialog showing the message for Failing to activate the App
	final public void displayFailedActivationMessage(String message)
	{
		
		// Dialog to display failed activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Failed");
		alert.setMessage(message);

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();
	}
}
