package com.eyeon.gane.services;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.eyeon.gane.utils.JsonParser;

public class Webservices
{

	// Class Variables
	String sVersion = "v1";
	String sVersion2 = "v2";
	String sLocalServer = "localhost:3000";
	String sStagingServer = "ganestage.herokuapp.com";
	String sLiveServer = "gane.herokuapp.com";

	String sWebserviceURL = "http://" +sLiveServer+ "/api/" +sVersion+ "/";
	String sWebserviceURL2 = "http://" +sStagingServer+ "/api/" +sVersion2+ "/";
	JsonParser oJsonParser;
	JSONArray oJSONArrayData;
	JSONObject oJSONObjectData;
	String sJsonData = "";

	String sWebserviceURLTerfLog = "http://" +sLiveServer+ "/api/" +sVersion2+ "/";

	// Flag
	boolean bHttpGet = true;
	boolean bSubmitQuizAnalytics = false;
	
	// Request Task
	RequestTask asyncTask;
	
	// Calling Activity
	Context mContext;

	// Constructor
	public Webservices(Context context)
	{
		
		mContext = context;
		
		oJsonParser = new JsonParser();
		oJSONArrayData = new JSONArray();
		oJSONObjectData = new JSONObject();
		asyncTask = new RequestTask();
	}

	//////////////////////// WEBSERVICE CALLS ////////////////////////
	
	//////////////////////// GET CALLS ////////////////////////
	
	// Get the Agency Categories
	public JSONObject getAgencyCategories()
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("agencies/categories"); 
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Agency Details by Id
	public JSONObject getAgencyById(String id)
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("agencies/"+id+"/detail"); 
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Quiz Categories
	public JSONObject getQuizCategories()
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("quizzes"); 
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Law Categories
	public JSONObject getLawCategories()
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("laws/categories"); 
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Law List and Details by Category Id
	public JSONObject getLawsByCategory(String cat_id)
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("laws/"+cat_id+"/list"); 
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Priority Numbers
	public JSONObject getPriorityNumbers()
	{
		// Raise Get Flag
		bHttpGet = true;
		
		// Build the URL
		String[] sUrl = new String[1];
		sUrl[0] = buildURL("contact_numbers");
		
		// Execute GET
		executeRequest(sUrl);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}

	//////////////////////// POST CALLS ////////////////////////
	
	// Submit the Device Token
	public JSONObject addDeviceToken(String token, String device_id)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = null;
		
		// Check if New install or Upgrade
		if (device_id == null || device_id == "")
			sData = new String[3];
		else
			sData = new String[5];

		// Insert basic Data
		sData[0] = buildURL("devices/new");
		sData[1] = "device_token";
		sData[2] = token;
		
		// Insert Upgrade Data
		if (!device_id.isEmpty())
		{
			sData[3] = "user_id_string";
			sData[4] = device_id;
		}

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Submit the Activation code
	public JSONObject postActivationCode(String[] data)
	{
		
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.length * 2) + 1];
		sData[0] = buildURL("activation_codes/authenticate"); 
		sData[1] = "code";
		sData[2] = data[0];
		sData[3] = "device_token";
		sData[4] = data[1];
		sData[5] = "user_type";
		sData[6] = data[2];
				
		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Create a New Client
	public JSONObject addClient(String[] data)
	{
		
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.length * 2) + 1];
		sData[0] = buildURL("clients/clients");
		sData[1] = "device_token";
		sData[2] = data[0];
		sData[2] = "age";
		sData[3] = data[1];
		sData[4] = "sex";
		sData[5] = data[2];
		sData[6] = "education";
		sData[7] = data[3];
		sData[8] = "race_ethnicity";
		sData[9] = data[4];
		sData[10] = "living_situation";
		sData[11] = data[5];
				
		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	public JSONObject makeReferral(String[] data)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[7];
		sData[0] = buildURL("clients/makeReferral"); 
		sData[1] = "device_token";
		sData[2] = data[0];
		sData[3] = "client_id";
		sData[4] = data[1];
		sData[5] = "quiz_session_id";
		sData[6] = data[0];
				
		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Refer the Client
	public JSONObject referClient(String[] data)
	{
		
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[3];
		sData[0] = buildURL("clients/referred"); 
		sData[1] = "client_referral_id";
		sData[2] = data[0];
				
		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Get the Quiz Section Questions by Quiz Id
	// Get next Section Questions if session_id is present
	public JSONObject getQuestionsByQuizId(String[] data)
	{
		// Raise Get Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.length * 2) + 1];
		sData[0] = buildURL("quizzes/questions"); 
		sData[1] = "device_token";
		sData[2] = data[0];
		sData[3] = "quiz_id";
		sData[4] = data[1];
		
		// Send Session ID for next Section's questions
		if (data.length > 2)
		{
			sData[5] = "session_id";
			sData[6] = data[2];
		}

		// Execute GET
		executeRequest(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	/*
	 * PRE-ANALYTICS QUIZ RESULTS SUBMISSION
	// Submit the Quiz results
	public JSONObject postQuizResults(String[] data)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.length * 2) + 1];
		sData[0] = buildURL("quizzes/results"); 
		sData[1] = data[0];
		sData[2] = data[1];
		sData[3] = data[2];
		sData[4] = data[3];
		
		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	*/
	
	
	
	// Submit the Quiz results
	public JSONObject postQuizResults(ArrayList<String[]> data)
	{
		// Raise Post Flag
		bHttpGet = false;
		bSubmitQuizAnalytics = true;
		
		// Build the URL
		String[] sDataPacket = new String[7];
		sDataPacket[0] = buildURL("quizzes/results");
		sDataPacket[1] = "device_token";
		sDataPacket[2] = data.get(0)[0];
		sDataPacket[3] = "session_id";
		sDataPacket[4] = data.get(0)[1];
		sDataPacket[5] = "quiz_id";
		sDataPacket[6] = data.get(0)[2];

		String[] answers = data.get(1);
		
		// Execute POST
		executeDataPost(sDataPacket, answers);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	
	
	
	// Get the Agency List by Category
	public JSONObject getAgenciesByCategory(ArrayList<String> data)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.size() * 2) + 1];
		sData[0] = buildURL("agencies/search"); 
		sData[1] = "category_id"; // category_id key
		sData[2] = data.get(0); // category_id value
		sData[3] = "latitude"; // latitude key
		sData[4] = data.get(1); // latitude value
		sData[5] = "longitude"; // longitude key
		sData[6] = data.get(2); // longitude value

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	/*
	// Get nearest location by Zip
	public JSONObject getEmergencyPlacementByZip(ArrayList<String> data)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.size() * 2) + 1];
		sData[0] = buildURL("agencies/search"); 
		sData[1] = "latitude"; // latitude key
		sData[2] = data.get(0); // latitude value
		sData[3] = "longitude"; // longitude key
		sData[4] = data.get(1); // longitude value
		sData[5] = "zip_code"; // zip_code key
		sData[6] = data.get(2); // zip_code value

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	*/
	
	// Post Mattie's Call information
	public JSONObject postMattiesCallInfo(ArrayList<String> data)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[(data.size() * 2) + 1];
		sData[0] = buildURL("matties_calls/new"); 
		sData[1] = "device_token"; // device token key
		sData[2] = data.get(0); // device token
		sData[3] = "reason"; // reason key
		sData[4] = data.get(1); // reason value

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}


	public JSONObject postLogInInfo(String data) throws JSONException
	{
		// Raise Post Flag
		bHttpGet = false;

		// Build the URL
		String[] sData = new String[5];
		sData[0] = buildURLTerf("events/record_event");
		sData[1] = "event_name"; // device token key
		sData[2] = "app_login";
		sData[3] = "device_token"; // device token key
		sData[4] = data; // device token


		Log.d("Value",sData[0]);
		// Execute POST
		executeDataPost(sData);

		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);

		// Return the Data
		return oJSONObjectData;
	}

	public JSONObject postTerf(String data) throws JSONException
	{
		// Raise Post Flag
		bHttpGet = false;

		// Build the URL
		String[] sData = new String[5];
		sData[0] = buildURLTerf("events/record_event");
		sData[1] = "event_name"; // device token key
		sData[2] = "terf_button";
		sData[3] = "device_token"; // device token key
		sData[4] = data; // device token



		Log.d("Value",sData[0]);
		// Execute POST
		executeDataPost(sData);

		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);

		// Return the Data
		return oJSONObjectData;
	}

	public JSONObject postContactUs(String data) throws JSONException
	{
		// Raise Post Flag
		bHttpGet = false;

		// Build the URL
		String[] sData = new String[5];
		sData[0] = buildURL("events/record_event");
		sData[1] = "event_name"; // device token key
		sData[2] = "contact_us";
		sData[3] = "device_token"; // device token key
		sData[4] = data; // device token



		Log.d("Value",sData[0]);
		// Execute POST
		executeDataPost(sData);

		// Convert the Results to JSON
		if(sJsonData!="") {
			oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		}
		// Return the Data
		return oJSONObjectData;
	}

	// Get nearest location by Zip
	public JSONObject getUnreadPushNotifications(String token)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[3];
		sData[0] = buildURL("push_notifications/unread"); 
		sData[1] = "device_token";
		sData[2] = token;

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	// Mark Notification as Read
	public JSONObject markNotificationRead(String token, String notification_id)
	{
		// Raise Post Flag
		bHttpGet = false;
		
		// Build the URL
		String[] sData = new String[5];
		sData[0] = buildURL("push_notifications/read"); 
		sData[1] = "device_token";
		sData[2] = token;
		sData[3] = "notification_id";
		sData[4] = notification_id;

		// Execute POST
		executeDataPost(sData);
		
		// Convert the Results to JSON
		oJSONObjectData = oJsonParser.convertStringToJson(sJsonData);
		
		// Return the Data
		return oJSONObjectData;
	}
	
	//////////////////////// CLASS METHODS ////////////////////////
	
	// Build Webservice URL
	private String buildURL(String extras)
	{
		return sWebserviceURL + extras;
	}

	private String buildURLTerf(String extras)
	{
		return sWebserviceURLTerfLog + extras;
	}

	private String buildURL1(String extras)
	{
		return sWebserviceURL2 + extras;
	}

	// Open another thread and execute the GET submission
	private void executeRequest(String[] url)
	{
		// Make HTTP Post Request in separate Thread and wait for its response
		try
		{
			asyncTask.execute(url).get();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		catch (ExecutionException e)
		{
			e.printStackTrace();
		}
		
		// Close the Async Task
		asyncTask.cancel(true);
	}
	
	// Open another thread and execute the POST submission
	private void executeDataPost(String[]... data)
	{
		// Make HTTP Post Request in separate Thread and wait for its response
		try 
		{
			asyncTask.execute(data).get();
		} 
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		} 
		catch (ExecutionException e) 
		{
			e.printStackTrace();
		}
		
		// Close the Async Task
		asyncTask.cancel(true);
	}

	/////////////// AsyncTask class for making GET/POST Calls //////////////////////////
	private class RequestTask extends AsyncTask<String[], String, Integer>
	{
		
		//////////////////////// ASYNCTASK VERSION ISSUE FIX ////////////////////////
		@SuppressWarnings("unused")
		public AsyncTask<String[], String, Integer> executeStart(String... data)
		{
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
			{
				return executePostHoneycomb(data);
			}
			else
			{
				return super.execute(data);
			}
		}
			
		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		private AsyncTask<String[], String, Integer> executePostHoneycomb(String... data)
		{
			return super.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, data);
		}
		////////////////////////////////////////////////////////////////////////////
		
		private ProgressDialog dialog = new ProgressDialog(mContext);
		
		@Override
	    protected void onPreExecute() 
		{
			// Check if context is Activity
			if (mContext instanceof Activity)
			{
				this.dialog.setMessage("Loading");
		        this.dialog.show();
			}
	    }
		
		@Override
		protected void onPostExecute(Integer result) 
		{

			// Check if context is Activity
			if (mContext instanceof Activity)
			{
				this.dialog.dismiss();
			}
			
			super.onPostExecute(result);
		}

		@Override
		protected Integer doInBackground(String[]... data) 
		{
			
			if (!isCancelled())
			{
				// Webservice Get Request Variables
				DefaultHttpClient oClient = new DefaultHttpClient(new BasicHttpParams());
				
				// Response Value
				String sResponse = null;
		
				// Section of Code for a GET Request
				if (bHttpGet)
				{
					
					BufferedReader in = null;

					try 
					{
						URI sURL = new URI(data[0][0]);
						HttpGet oGetRequest = new HttpGet();
						oGetRequest.setURI(sURL);
						HttpResponse response = oClient.execute(oGetRequest);
			            response.getStatusLine().getStatusCode();

			            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			            StringBuffer sb = new StringBuffer("");
			            String l = "";
			            String nl = System.getProperty("line.separator");
			            while ((l = in.readLine()) !=null)
			            {
			                sb.append(l + nl);
			            }
			            in.close();
			            sResponse = sb.toString();
			            sJsonData = sResponse;
			            onPostExecute(1);
			            return 1;        
			        } 
					catch (URISyntaxException e) 
			        {
						e.printStackTrace();
					} 
					catch (ClientProtocolException e) 
					{
						e.printStackTrace();
					} 
					catch (IOException e) 
					{
						e.printStackTrace();
					} 
					finally
					{
			            if (in != null)
			            {
			                try
			                {
			                    in.close();
			                    sJsonData = sResponse;
			                    onPostExecute(1);
			                    return 1;
			                }
			                catch (Exception e)
			                {
			                    e.printStackTrace();
			                }
			            }
			        }
				}
				else
				{
					
					// Make a POST Request
					HttpPost oPost = new HttpPost(data[0][0]);
					oPost.setHeader("Content-type", "application/json");

					InputStream oIS = null;
					
					try
					{
						// Check if POST is for Quiz Analytics
						// Else POST Data normally
						if (bSubmitQuizAnalytics)
						{
							
							// Create JSON Packet
							JSONObject oJSONData = new JSONObject();

							// Add the Data to the JSON
							for (int i=1; i<data[0].length; i=i+2)
							{
								oJSONData.put(data[0][i], data[0][i+1]);
							}
							
							// Create Answers Array
							JSONArray oJSONAnswers = new JSONArray();
							
							// Loop through data adding question id and answer value to the Array
							for (int i=0; i<data[1].length; i=i+2)
							{
								JSONObject oJSONAnswerObjects = new JSONObject();
								oJSONAnswerObjects.put("answer", data[1][i+1]);
								oJSONAnswerObjects.put("question_id", data[1][i]);
								oJSONAnswers.put(oJSONAnswerObjects);
							}
							
							// Put the Answers Array in the JSONData Object
							oJSONData.put("answers", oJSONAnswers);

							// Get the String value of the JSONArray
							String jsonData = oJSONData.toString();
							
							// Attach the JSONArray Package to the POST Entity
							oPost.setEntity(new StringEntity(jsonData));
						}
						else if (data[0].length > 1)
						{
							// Create new JSON Object
							JSONObject oJSON = new JSONObject();

							// Add the Data to the JSON
							for (int i=1; i<data[0].length; i=i+2)
							{
								oJSON.put(data[0][i], data[0][i+1]);
							}

							// Attach the JSONObject Package to the POST Entity
							oPost.setEntity(new StringEntity(oJSON.toString()));
						}
					
						// Make Call
						HttpResponse oResponse = oClient.execute(oPost);
						HttpEntity oEntity = oResponse.getEntity();
						
						// Get Content
						oIS = oEntity.getContent();
						
						// Read in Data
						BufferedReader brReader = new BufferedReader(new InputStreamReader(oIS, "UTF-8"), 8);
						StringBuilder oSBuilder = new StringBuilder();
						
						try
						{
							// Reader line
							String line = null;
							
							// Write Data out to String
							while ((line = brReader.readLine()) != null)
							{
								oSBuilder.append(line + "\n");
							}
						}
						catch (Exception e)
						{
							Log.e("Buffer Error", "Error converting result " + e.toString());
						}

						// Get the Response String
						sResponse = oSBuilder.toString();
					}
					catch (Exception e)
					{
						// Throw Exception
						Log.e("POST Request Fail", "Error: " + e.toString());
					}
					finally
					{
						try
						{
							// Close the Stream
							if (oIS != null)
								oIS.close();
						}
						catch (Exception e)
						{
							Log.e("Unable Close In Stream", "Error: " + e.toString());
						}
					}
				}

				// Return the Response
				sJsonData = sResponse;
				
				/*
				// Stop the Progress Spinner
				if (mContext instanceof Activity)
				{
					this.dialog.dismiss();
				}
				*/
				
				onPostExecute(1);

				// Return response
				return 1;
			}
			else
			{
				// Try and Kill the Thread
				cancel(true);
			}
			
			// Default Return
			return 1;
		}
		
		@Override
		public void onCancelled(Integer result)
		{
			cancel(true);
		}
	}
}





