package com.eyeon.gane;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.StateSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import org.json.JSONObject;

import java.util.ArrayList;

public class Quiz extends AppCompatActivity implements OnClickListener {

    // Activity Variables
    Bundle savedData = new Bundle();
    String sType = "";
    String sQuizId = "";
    String sSessionId = "";
    String sRegId = "";
    boolean bIsFullVersion = false;

    // Activation Validation
    Spinner sprUserType = null;

    // Server Data
    ArrayList<String[]> alQuestionsData; // Contains all the Data
    ArrayList<String> alQuestions;         // Contains only the Questions

    // Quiz Variables
    ScrollView svQuizScrollView;

    // Client Info Container
    EditText etClientName;
    EditText etClientPhone;
    EditText etAge;
    CheckBox cbSexMale;
    CheckBox cbSexFemale;
    CheckBox cbEducation8th;
    CheckBox cbEducation9th;
    CheckBox cbEducation12th;
    CheckBox cbEducationSomeCollege;
    CheckBox cbEducationCollegeDegree;
    CheckBox cbEducationPostCollege;
    CheckBox cbRaceEthnicityAfricanAmerican;
    CheckBox cbRaceEthnicityAmericanIndian;
    CheckBox cbRaceEthnicityAsian;
    CheckBox cbRaceEthnicityCaucasian;
    CheckBox cbRaceEthnicityHispanic;
    CheckBox cbLivingSituationLivesAlone;
    CheckBox cbLivingSituationLivesWithOthers;
    Button btnSubmitClientInfo;
    int iSlideshowCounter = 0;
    int iSectionCounter = 0;
    boolean isSlideshow = false;
    String sSectionName = "Section 1";
    double iScalar = 1.0;
    String sScoreMetric = "Positive";

    Button btnCorrectAnswer;
    Button btnIncorrectAnswer;

    boolean bClientInfoPage = true;
    int iClientId = 0;
    int iClientReferralId = 0;

    boolean isQuestionTime = false;
    LinearLayout llQuestionSlideshowAnswerContainer;

    // Question Container
    QuestionContainer oQuestionContainer;
    Button btnSubmit;
    String[] answersData;

    // Results Container
    LinearLayout llResultContainer;
    TextView tvQuizResults;
    Button btnMakeReferral;

    // Graphics
    Drawable buttonTouch;
    Drawable buttonNoTouch;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Get saved state and call super
        savedData = savedInstanceState;
        super.onCreate(savedData);

        // Get Intent and pull extra to determine which screen to draw
        Intent intent = getIntent();
        sType = intent.getStringExtra("type");
        bIsFullVersion = intent.getBooleanExtra("version", false);
        sQuizId = intent.getStringExtra("quiz_id");

        String getClientInfo = intent.getStringExtra("get_client_info");
        if (getClientInfo.contains("true"))
            bClientInfoPage = true;
        else
            bClientInfoPage = false;

        // Initialize Questions ArrayList
        oQuestionContainer = new QuestionContainer();
        alQuestions = new ArrayList<String>();

        // Open a Connection to the DB, get the User Registration ID and close the connection
        DatabaseHandler db = new DatabaseHandler(this);
        sRegId = db.getRegId();
        db.close();

        // Get Screen resolution to update scalar
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;

        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;

        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;

        double diagonalInches = Math.sqrt((widthInches * widthInches) + (heightInches * heightInches));

        if (diagonalInches >= 7)
            iScalar = 2.0;

        if (bClientInfoPage) {
            // Set the Page Title
            setContentView(R.layout.get_client_info);

            // COmmented out by Alfred based on client requirement
            // Remove Name and Phone fields from Financial Screening's first screen

            //etClientName = (EditText)findViewById(R.id.etName);
            //etClientPhone = (EditText)findViewById(R.id.etPhone);
            etAge = (EditText) findViewById(R.id.etAge);
            cbSexMale = (CheckBox) findViewById(R.id.cbSexMale);
            cbSexFemale = (CheckBox) findViewById(R.id.cbSexFemale);
            cbEducation8th = (CheckBox) findViewById(R.id.cbEducation8thGradeOrLess);
            cbEducation9th = (CheckBox) findViewById(R.id.cbEducation9thTo11thGrade);
            cbEducation12th = (CheckBox) findViewById(R.id.cbEducation12th);
            cbEducationSomeCollege = (CheckBox) findViewById(R.id.cbEducationSomeCollege);
            cbEducationCollegeDegree = (CheckBox) findViewById(R.id.cbEducationCollegeDegree);
            cbEducationPostCollege = (CheckBox) findViewById(R.id.cbEducationPostCollege);
            cbRaceEthnicityAfricanAmerican = (CheckBox) findViewById(R.id.cbRaceEthnicityAfricanAmerican);
            cbRaceEthnicityAmericanIndian = (CheckBox) findViewById(R.id.cbRaceEthnicityAmericanIndian);
            cbRaceEthnicityAsian = (CheckBox) findViewById(R.id.cbRaceEthnicityAsian);
            cbRaceEthnicityCaucasian = (CheckBox) findViewById(R.id.cbRaceEthnicityCaucasian);
            cbRaceEthnicityHispanic = (CheckBox) findViewById(R.id.cbRaceEthnicityHispanic);
            cbLivingSituationLivesAlone = (CheckBox) findViewById(R.id.cbLivingSituationLivesAlone);
            cbLivingSituationLivesWithOthers = (CheckBox) findViewById(R.id.cbLivingSituationLivesWithOthers);

            cbSexMale.setOnClickListener(this);
            cbSexFemale.setOnClickListener(this);
            cbEducation8th.setOnClickListener(this);
            cbEducation9th.setOnClickListener(this);
            cbEducation12th.setOnClickListener(this);
            cbEducationSomeCollege.setOnClickListener(this);
            cbEducationCollegeDegree.setOnClickListener(this);
            cbEducationPostCollege.setOnClickListener(this);
            cbRaceEthnicityAfricanAmerican.setOnClickListener(this);
            cbRaceEthnicityAmericanIndian.setOnClickListener(this);
            cbRaceEthnicityAsian.setOnClickListener(this);
            cbRaceEthnicityCaucasian.setOnClickListener(this);
            cbRaceEthnicityHispanic.setOnClickListener(this);
            cbLivingSituationLivesAlone.setOnClickListener(this);
            cbLivingSituationLivesWithOthers.setOnClickListener(this);

            cbSexMale.setTextSize((float) (12 * iScalar));
            cbSexFemale.setTextSize((float) (12 * iScalar));
            cbEducation8th.setTextSize((float) (12 * iScalar));
            cbEducation9th.setTextSize((float) (12 * iScalar));
            cbEducation12th.setTextSize((float) (12 * iScalar));
            cbEducationSomeCollege.setTextSize((float) (12 * iScalar));
            cbEducationCollegeDegree.setTextSize((float) (12 * iScalar));
            cbEducationPostCollege.setTextSize((float) (12 * iScalar));
            cbRaceEthnicityAfricanAmerican.setTextSize((float) (12 * iScalar));
            cbRaceEthnicityAmericanIndian.setTextSize((float) (12 * iScalar));
            cbRaceEthnicityAsian.setTextSize((float) (12 * iScalar));
            cbRaceEthnicityCaucasian.setTextSize((float) (12 * iScalar));
            cbRaceEthnicityHispanic.setTextSize((float) (12 * iScalar));
            cbLivingSituationLivesAlone.setTextSize((float) (12 * iScalar));
            cbLivingSituationLivesWithOthers.setTextSize((float) (12 * iScalar));

            // Get the Button NoTouch/Touch Graphics
            buttonTouch = getResources().getDrawable(R.drawable.button_touch);
            buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

            // Create the clickable Effects
            StateListDrawable ButtonEffects = new StateListDrawable();
            ButtonEffects.addState(new int[]{android.R.attr.state_pressed}, buttonTouch);
            ButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

            btnSubmitClientInfo = (Button) findViewById(R.id.btnSubmitClientInfo);
            btnSubmitClientInfo.setBackground(ButtonEffects);
            btnSubmitClientInfo.setOnClickListener(this);
            btnSubmitClientInfo.setTextSize((float) (14 * iScalar));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Only display in Free Version
        if (!bIsFullVersion) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.activation, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Create Dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        // Set title and message
        alert.setTitle("Activate Personnel Version");
        alert.setMessage("Enter Activation Code");

        // String Array of the User Types
        String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};

        // Create LinearLayout
        LinearLayout activationLayout = new LinearLayout(this);
        activationLayout.setOrientation(LinearLayout.VERTICAL);

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        sprUserType = new Spinner(this);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sprUserType.setAdapter(spinnerArrayAdapter);

        // Add Widgets to Layout
        activationLayout.addView(input);
        activationLayout.addView(sprUserType);

        // Set the Dialog's View
        alert.setView(activationLayout); //input);

        // Initialize the Webservice
        final Webservices activationCall = new Webservices(this);
        DatabaseHandler dbHandler = new DatabaseHandler(this);
        final String apid = dbHandler.getRegId();
        dbHandler.close();

        // Set positive button - Listener is Null, overriden below
        alert.setPositiveButton("Ok", null);

        // Set negative button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Create the Alert Dialog
        final AlertDialog activationDialog = alert.create();

        // Set the Dialog Show Listener to enforce Validation on the User Role Selection
        activationDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                // Get the Positive Button
                Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);

                // Add a Click Listener
                positiveButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {

                        // Get the value
                        Editable value = input.getText();
                        String userRole = sprUserType.getSelectedItem().toString();

                        if (!userRole.contains("Select")) {
                            // Create the Array and add the values to the array
                            String[] data = new String[3];
                            data[0] = value.toString();    // Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)

                            // Get the APID
                            data[1] = apid; // Device Token  (Urban Airship APID)

                            // Get the UserType
                            data[2] = userRole;

                            // Get the results
                            JSONObject results = activationCall.postActivationCode(data);

                            // Parse the Data and return an ArrayList of Questions
                            JsonParser parser = new JsonParser();
                            String[] activationResults = parser.checkPostActivationCodeSubmission(results);

                            if (activationResults.length == 1) {
                                // Activate the App
                                activateApp(data[0]);
                            } else {
                                // Activation Failed - Display Fail Message
                                displayFailedActivationMessage(activationResults[1]);
                            }

                            // Close the Dialog
                            activationDialog.dismiss();
                        } else {
                            // Display Invalid Data Notification
                            Toast.makeText(Quiz.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
                        }

                    }
                });
            }
        });

        // Show Dialog
        activationDialog.show();

        // Return True
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        // UrbanAirship
        //if (UAirship.shared().isFlying())
        //	UAirship.shared().getAnalytics().activityStarted(this);
    }

    @Override
    protected void onResume() {

        if (savedData != null)
            onRestoreInstanceState(savedData);

        if (!isQuestionTime) {
            getQuestionSections();
            isQuestionTime = true;
        }

        if (!bClientInfoPage && !isSlideshow) {
            // Set the ContentView and the Page Title
            setContentView(R.layout.quiz);
            setTitle(sType);

            // Get Section Questions
            buildQuestionsArray();

            // Get the Widgets
            TextView tvQuizInstructions = (TextView) findViewById(R.id.tvQuizInstructions);

            // Set the Instruction Text based on the QuizType TODO: Change this to Public Safety
            if (sType.toLowerCase().contains("cognitive"))
                tvQuizInstructions.setText("Check if INCORRECTLY answered");
            else
                tvQuizInstructions.setText("Check if answer is Yes");

            // Build Quiz
            buildQuiz();
        } else if (!bClientInfoPage && isSlideshow) {

            if (alQuestions.isEmpty()) {
                // Set the ContentView and the Page Title
                setContentView(R.layout.quiz_slideshow);
                setTitle(sType);

                // Get Section Questions
                buildQuestionsArray();

                answersData = new String[alQuestions.size() * 2];

                iSectionCounter += 1;

                // Build Quiz
                buildQuizSlideshow();
            } else {
                if (alQuestions.size() == iSlideshowCounter) {
                    iSlideshowCounter = 0;

                    // Get Section Questions
                    buildQuestionsArray();

                    answersData = new String[alQuestions.size() * 2];

                    // Build Quiz
                    buildQuizSlideshow();
                } else if (alQuestions.size() > iSlideshowCounter) {
                    // Build Quiz
                    buildQuizSlideshow();
                }
            }
        }

        // Resume the App
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (savedData != null)
            onSaveInstanceState(savedData);

        super.onPause();
    }

    @Override
    protected void onStop() {
        if (savedData != null)
            onSaveInstanceState(savedData);

        // UrbanAirship
        //if (UAirship.shared().isFlying())
        //	UAirship.shared().getAnalytics().activityStopped(this);

        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Destroy the Activity
        finish();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        savedInstanceState.putString("type", sType);
        savedInstanceState.putBoolean("version", bIsFullVersion);
        savedInstanceState.putString("quiz_id", sQuizId);

        // Call Super Class
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {

        sType = savedInstanceState.getString("type");
        bIsFullVersion = savedInstanceState.getBoolean("version", false);
        sQuizId = savedInstanceState.getString("quiz_id");

        // Call Super Class
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

        switch (v.getId()) {
            case R.id.cbSexMale:
                cbSexMale.setChecked(true);
                cbSexFemale.setChecked(false);
                break;
            case R.id.cbSexFemale:
                cbSexFemale.setChecked(true);
                cbSexMale.setChecked(false);
                break;
            case R.id.cbEducation8thGradeOrLess:
                cbEducation8th.setChecked(true);
                cbEducation9th.setChecked(false);
                cbEducation12th.setChecked(false);
                cbEducationSomeCollege.setChecked(false);
                cbEducationCollegeDegree.setChecked(false);
                cbEducationPostCollege.setChecked(false);
                break;
            case R.id.cbEducation9thTo11thGrade:
                cbEducation8th.setChecked(false);
                cbEducation9th.setChecked(true);
                cbEducation12th.setChecked(false);
                cbEducationSomeCollege.setChecked(false);
                cbEducationCollegeDegree.setChecked(false);
                cbEducationPostCollege.setChecked(false);
                break;
            case R.id.cbEducation12th:
                cbEducation8th.setChecked(false);
                cbEducation9th.setChecked(false);
                cbEducation12th.setChecked(true);
                cbEducationSomeCollege.setChecked(false);
                cbEducationCollegeDegree.setChecked(false);
                cbEducationPostCollege.setChecked(false);
                break;
            case R.id.cbEducationSomeCollege:
                cbEducation8th.setChecked(false);
                cbEducation9th.setChecked(false);
                cbEducation12th.setChecked(false);
                cbEducationSomeCollege.setChecked(true);
                cbEducationCollegeDegree.setChecked(false);
                cbEducationPostCollege.setChecked(false);
                break;
            case R.id.cbEducationCollegeDegree:
                cbEducation8th.setChecked(false);
                cbEducation9th.setChecked(false);
                cbEducation12th.setChecked(false);
                cbEducationSomeCollege.setChecked(false);
                cbEducationCollegeDegree.setChecked(true);
                cbEducationPostCollege.setChecked(false);
                break;
            case R.id.cbEducationPostCollege:
                cbEducation8th.setChecked(false);
                cbEducation9th.setChecked(false);
                cbEducation12th.setChecked(false);
                cbEducationSomeCollege.setChecked(false);
                cbEducationCollegeDegree.setChecked(false);
                cbEducationPostCollege.setChecked(true);
                break;
            case R.id.cbRaceEthnicityAfricanAmerican:
                cbRaceEthnicityAfricanAmerican.setChecked(true);
                cbRaceEthnicityAmericanIndian.setChecked(false);
                cbRaceEthnicityAsian.setChecked(false);
                cbRaceEthnicityCaucasian.setChecked(false);
                cbRaceEthnicityHispanic.setChecked(false);
                break;
            case R.id.cbRaceEthnicityAmericanIndian:
                cbRaceEthnicityAfricanAmerican.setChecked(false);
                cbRaceEthnicityAmericanIndian.setChecked(true);
                cbRaceEthnicityAsian.setChecked(false);
                cbRaceEthnicityCaucasian.setChecked(false);
                cbRaceEthnicityHispanic.setChecked(false);
                break;
            case R.id.cbRaceEthnicityAsian:
                cbRaceEthnicityAfricanAmerican.setChecked(false);
                cbRaceEthnicityAmericanIndian.setChecked(false);
                cbRaceEthnicityAsian.setChecked(true);
                cbRaceEthnicityCaucasian.setChecked(false);
                cbRaceEthnicityHispanic.setChecked(false);
                break;
            case R.id.cbRaceEthnicityCaucasian:
                cbRaceEthnicityAfricanAmerican.setChecked(false);
                cbRaceEthnicityAmericanIndian.setChecked(false);
                cbRaceEthnicityAsian.setChecked(false);
                cbRaceEthnicityCaucasian.setChecked(true);
                cbRaceEthnicityHispanic.setChecked(false);
                break;
            case R.id.cbRaceEthnicityHispanic:
                cbRaceEthnicityAfricanAmerican.setChecked(false);
                cbRaceEthnicityAmericanIndian.setChecked(false);
                cbRaceEthnicityAsian.setChecked(false);
                cbRaceEthnicityCaucasian.setChecked(false);
                cbRaceEthnicityHispanic.setChecked(true);
                break;
            case R.id.cbLivingSituationLivesAlone:
                cbLivingSituationLivesAlone.setChecked(true);
                cbLivingSituationLivesWithOthers.setChecked(false);
                break;
            case R.id.cbLivingSituationLivesWithOthers:
                cbLivingSituationLivesAlone.setChecked(false);
                cbLivingSituationLivesWithOthers.setChecked(true);
                break;
            case R.id.btnSubmitClientInfo:

                //String name = etClientName.getText().toString();
                //String phone = etClientPhone.getText().toString();
                String age = etAge.getText().toString();

                String sex = "";
                if (cbSexMale.isChecked()) {
                    sex = "Male";
                } else if (cbSexFemale.isChecked()) {
                    sex = "Female";
                }

                String education = "";
                if (cbEducation8th.isChecked()) {
                    education = "8th grade or less";
                } else if (cbEducation9th.isChecked()) {
                    education = "9th to 11th grade";
                } else if (cbEducation12th.isChecked()) {
                    education = "12th grade (High School Diploma) or GED";
                } else if (cbEducationSomeCollege.isChecked()) {
                    education = "Some College";
                } else if (cbEducationPostCollege.isChecked()) {
                    education = "Post College (Graduate school) (more than 16 years)";
                }

                String race_ethnicity = "";
                if (cbRaceEthnicityAfricanAmerican.isChecked()) {
                    race_ethnicity = "African-American";
                } else if (cbRaceEthnicityAmericanIndian.isChecked()) {
                    race_ethnicity = "American Indian";
                } else if (cbRaceEthnicityAsian.isChecked()) {
                    race_ethnicity = "Asian";
                } else if (cbRaceEthnicityCaucasian.isChecked()) {
                    race_ethnicity = "Caucasian";
                } else if (cbRaceEthnicityHispanic.isChecked()) {
                    race_ethnicity = "Hispanic";
                }

                String living_situation = "";
                if (cbLivingSituationLivesAlone.isChecked()) {
                    living_situation = "Lives alone";
                } else if (cbLivingSituationLivesWithOthers.isChecked()) {
                    living_situation = "Lives with others";
                }

                String[] Client = {sRegId, age, sex, education, race_ethnicity, living_situation};

                Webservices AddClient = new Webservices(this);
                JSONObject oClientData = AddClient.addClient(Client);

                JsonParser parser1 = new JsonParser();
                iClientId = parser1.getAddClientResultsFromJson(oClientData);
                bClientInfoPage = false;

                onResume();

                break;
            case R.id.btnCorrectAnswer:
                // Waterfall
            case R.id.btnIncorrectAnswer:

                // Update Values
                iSlideshowCounter += 1;

                int data_counter = 0;
                int questionDataBuffer = 3; // To bypass session_id, is_slide_show and section_name
                for (String data : answersData) {
                    if (data == null) {
                        answersData[data_counter] = alQuestionsData.get(questionDataBuffer + iSlideshowCounter)[0];

                        if (sScoreMetric.contains("Positive")) {
                            if (v.getId() == R.id.btnCorrectAnswer)
                                answersData[data_counter + 1] = alQuestionsData.get(questionDataBuffer + iSlideshowCounter)[9];
                            else
                                answersData[data_counter + 1] = String.valueOf(0);
                        } else {
                            if (v.getId() == R.id.btnIncorrectAnswer)
                                answersData[data_counter + 1] = alQuestionsData.get(questionDataBuffer + iSlideshowCounter)[9];
                            else
                                answersData[data_counter + 1] = String.valueOf(0);
                        }

                        break;
                    }

                    data_counter += 1;
                }

                if (alQuestions.size() > iSlideshowCounter) {
                    onResume();
                } else if (alQuestions.size() == iSlideshowCounter) {

                    // Make Webservice Call to get list of Questions
                    Webservices Questions = new Webservices(this);
                    String[] data = new String[3];
                    data[0] = sRegId;
                    data[1] = sSessionId;
                    data[2] = sQuizId;

                    // Make JSON Package
                    ArrayList<String[]> jsonPackage = new ArrayList<String[]>();
                    jsonPackage.add(data);
                    jsonPackage.add(answersData);

                    // Get the results
                    JSONObject results = Questions.postQuizResults(jsonPackage);

                    // Parse the Data and return the score results
                    JsonParser parser = new JsonParser();
                    ArrayList<String> saQuizResults = parser.getQuizResultsFromJson(results);

                    // Check Pass/Fail flag and draw Results accordingly
                    if (saQuizResults.get(0).contains("Next Section")) {
                        /////////// NEXT SECTION ///////////
                        isQuestionTime = false;
                        onResume();
                    } else if (saQuizResults.get(0).contains("fail")) {

                        /////////// FAILED THE QUIZ ///////////
                        // Display Results
                        LinearLayout llQuestionContainer = (LinearLayout) findViewById(R.id.llQuizSlideshowQuestionContainer);
                        llQuestionContainer.setVisibility(View.GONE);

                        Button btnCorrect = (Button) findViewById(R.id.btnCorrectAnswer);
                        Button btnIncorrect = (Button) findViewById(R.id.btnIncorrectAnswer);

                        btnCorrect.setVisibility(View.GONE);
                        btnIncorrect.setVisibility(View.GONE);

                        llResultContainer = (LinearLayout) findViewById(R.id.quizSlideshowResultContainer);
                        llResultContainer.setVisibility(View.VISIBLE);

                        // Set the Correct Text to display depending on the Quiz Type
                        tvQuizResults = (TextView) findViewById(R.id.tvSlideshowQuizResults);
                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        // Set the Correct Text to display depending on the Quiz Type
                        if (sType.toLowerCase().contains("financial")) {
                            String FC_Fail_Text = "A referral for an expanded DFC/FE assessment is recommended."
                                    + "Please click on the Make Referral button below to call APS.";
                            tvQuizResults.setText(FC_Fail_Text);
                        } else if (sType.toLowerCase().contains("cognitive"))
                            tvQuizResults.setText(R.string.CognitiveResultNotOk);
                        else if (sType.toLowerCase().contains("abuse"))
                            tvQuizResults.setText(R.string.AbuseResultNotOk);
                        else if (sType.toLowerCase().contains("neglect")) {
                            // Check for Self Neglect
                            String is_self_neglect = saQuizResults.get(1);
                            boolean self_neglect = false;

                            if (is_self_neglect.contains("yes"))
                                self_neglect = true;

                            if (self_neglect)
                                tvQuizResults.setText(R.string.NeglectResultNotOkWithSelfNeglect);
                            else
                                tvQuizResults.setText(R.string.NeglectResultNotOkWithoutSelfNeglect);
                        } else if (sType.toLowerCase().contains("exploitation"))
                            tvQuizResults.setText(R.string.ExploitationResultNotOk);

                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        tvQuizResults.setVisibility(View.VISIBLE);

                        // Make Button Visible
                        btnMakeReferral = (Button) findViewById(R.id.bSlideshowMakeReferral);
                        btnMakeReferral.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        btnMakeReferral.setVisibility(View.VISIBLE);
                        btnMakeReferral.setOnClickListener(this);

                        Webservices webMakeReferral = new Webservices(this);
                        String[] referralInfo = {sRegId, iClientId + "", sSessionId};
                        JSONObject oReferralResults = webMakeReferral.makeReferral(referralInfo);

                        JsonParser referral_parser = new JsonParser();
                        iClientReferralId = referral_parser.getMakeReferralResultsFromJson(oReferralResults);

                        // Reset the Session Id and disable the Submit Button
                        sSessionId = "";
                    } else if (!saQuizResults.get(0).contains("Next Section") && !saQuizResults.get(0).contains("fail")) {

                        /////////// PASSED THE QUIZ ///////////
                        // Display Results
                        LinearLayout llQuestionContainer = (LinearLayout) findViewById(R.id.llQuizSlideshowQuestionContainer);
                        llQuestionContainer.setVisibility(View.GONE);

                        Button btnCorrect = (Button) findViewById(R.id.btnCorrectAnswer);
                        Button btnIncorrect = (Button) findViewById(R.id.btnIncorrectAnswer);

                        btnCorrect.setVisibility(View.GONE);
                        btnIncorrect.setVisibility(View.GONE);

                        TextView tvQuizResultLabel = (TextView) findViewById(R.id.tvSectionName);
                        tvQuizResultLabel.setText("Quiz Results");

                        llResultContainer = (LinearLayout) findViewById(R.id.quizSlideshowResultContainer);
                        llResultContainer.setVisibility(View.VISIBLE);

                        tvQuizResults = (TextView) findViewById(R.id.tvSlideshowQuizResults);
                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));

                        // Set the Correct Text to display depending on the Quiz Type
                        if (sType.toLowerCase().contains("financial"))
                            tvQuizResults.setText("No referral for Financial Capacity needs to be made at this time.");
                        else if (sType.toLowerCase().contains("cognitive"))
                            tvQuizResults.setText(R.string.CognitiveResultOk);
                        else if (sType.toLowerCase().contains("abuse"))
                            tvQuizResults.setText(R.string.AbuseResultOk);
                        else if (sType.toLowerCase().contains("neglect"))
                            tvQuizResults.setText(R.string.NeglectResultOk);
                        else if (sType.toLowerCase().contains("exploitation"))
                            tvQuizResults.setText(R.string.ExploitationResultOk);

                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        tvQuizResults.setVisibility(View.VISIBLE);

                        // Reset the Session Id and disable the Submit Button
                        sSessionId = "";
                    }
                }

                break;
            case R.id.bSubmitQuiz:

                // Make Webservice Call to get list of Questions
                Webservices Questions = new Webservices(this);
                String[] data = new String[6];
                data[0] = sRegId;
                data[1] = sSessionId;
                data[2] = sQuizId;

                // Create the Array of Answer Data
                answersData = new String[oQuestionContainer.getQuizCount() * 2];
                int count = 0;

                // Loop through the Questions Container and get the Values from each Question
                for (Question quest : oQuestionContainer.getQuestionContainer()) {

                    // Get the Question Id
                    answersData[count] = String.valueOf(quest.getId());

                    // Get the Answer Value
                    if (quest.getCheckedStatus()) {
                        if (sScoreMetric.contains("Positive"))
                            answersData[count + 1] = String.valueOf(quest.getPointValue());
                        else
                            answersData[count + 1] = String.valueOf(0);
                    } else {
                        if (sScoreMetric.contains("Negative"))
                            answersData[count + 1] = String.valueOf(quest.getPointValue());
                        else
                            answersData[count + 1] = String.valueOf(0);
                    }

                    // Increment Counter
                    count += 2;
                }

                // Make JSON Package
                ArrayList<String[]> jsonPackage = new ArrayList<String[]>();
                jsonPackage.add(data);
                jsonPackage.add(answersData);

                // Get the results
                JSONObject results = Questions.postQuizResults(jsonPackage);

                // Parse the Data and return the score results
                JsonParser parser = new JsonParser();
                ArrayList<String> saQuizResults = parser.getQuizResultsFromJson(results);

                // Check Pass/Fail flag and draw Results accordingly

                if (saQuizResults != null && saQuizResults.size() > 0) {
                    if (!saQuizResults.get(0).contains("fail")) {

                        /////////// PASSED THE QUIZ ///////////
                        // Display Results
                        llResultContainer.setVisibility(View.VISIBLE);

                        // Set the Correct Text to display depending on the Quiz Type
                        if (sType.toLowerCase().contains("cognitive"))
                            tvQuizResults.setText(R.string.CognitiveResultOk);
                        else if (sType.toLowerCase().contains("abuse"))
                            tvQuizResults.setText(R.string.AbuseResultOk);
                        else if (sType.toLowerCase().contains("neglect"))
                            tvQuizResults.setText(R.string.NeglectResultOk);
                        else if (sType.toLowerCase().contains("exploitation"))
                            tvQuizResults.setText(R.string.ExploitationResultOk);

                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        tvQuizResults.setVisibility(View.VISIBLE);

                        // Move focus to bottom of screen
                        svQuizScrollView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svQuizScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        }, 1);
                    } else {

                        /////////// FAILED THE QUIZ ///////////
                        // Display Results
                        llResultContainer.setVisibility(View.VISIBLE);

                        // Set the Correct Text to display depending on the Quiz Type
                        if (sType.toLowerCase().contains("cognitive"))
                            tvQuizResults.setText(R.string.CognitiveResultNotOk);
                        else if (sType.toLowerCase().contains("abuse"))
                            tvQuizResults.setText(R.string.AbuseResultNotOk);
                        else if (sType.toLowerCase().contains("neglect")) {
                            // Check for Self Neglect
                            String is_self_neglect = saQuizResults.get(1);
                            boolean self_neglect = false;

                            if (is_self_neglect.contains("yes"))
                                self_neglect = true;

                            if (self_neglect)
                                tvQuizResults.setText(R.string.NeglectResultNotOkWithSelfNeglect);
                            else
                                tvQuizResults.setText(R.string.NeglectResultNotOkWithoutSelfNeglect);
                        } else if (sType.toLowerCase().contains("exploitation"))
                            tvQuizResults.setText(R.string.ExploitationResultNotOk);

                        //tvQuizResults.setText(R.string.CognitiveResultNotOk);
                        tvQuizResults.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        tvQuizResults.setVisibility(View.VISIBLE);

                        // Make Button Visible
                        btnMakeReferral.setTextColor(getResources().getColor(R.color.custom_theme_color));
                        btnMakeReferral.setVisibility(View.VISIBLE);

				/*
				Webservices webMakeReferral = new Webservices(this);
				
				String[] referralInfo = { sRegId, iClientId + "", sSessionId };
				JSONObject oReferralResults = webMakeReferral.makeReferral(referralInfo);
				
				JsonParser referral_parser = new JsonParser();
				iClientReferralId = referral_parser.getMakeReferralResultsFromJson(oReferralResults);
				*/

                        // Move focus to bottom of screen
                        svQuizScrollView.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                svQuizScrollView.fullScroll(ScrollView.FOCUS_DOWN);
                            }
                        }, 1);
                    }
                }
                // Reset the Session Id and disable the Submit Button
                sSessionId = "";
                btnSubmit.setVisibility(View.GONE);

                break;
            case R.id.bMakeReferral:

                // Call an Agency based on the Quiz Type
                if (sType.toLowerCase().contains("financial")) {
                    contactAPS();
                } else if (sType.toLowerCase().contains("cognitive")) {
                    // Call Alzheimers Association to make a Referral
                    contactAlzheimersAssociation();
                } else if (sType.toLowerCase().contains("abuse") || sType.toLowerCase().contains("neglect") || sType.toLowerCase().contains("exploitation")) {
                    // Call APS to make a Referral
                    contactAPS();
                }

                break;
            case R.id.bSlideshowMakeReferral:

                // Instantiate Webservice
                final Webservices callMakeReferral = new Webservices(this);
                final Webservices callAPS = new Webservices(this);

                // Dialog requesting permission to make Phone call
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setMessage("Call Adult Protective Services?");

                // Positive Button
                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        // Make Referral
                        String[] referral_id = {iClientReferralId + ""};
                        JSONObject oJson = callMakeReferral.referClient(referral_id);

                        JsonParser parser = new JsonParser();
                        boolean results = parser.getReferClientResultsFromJson(oJson);

                        if (results)
                            Log.i("Client Referred", "Referral ID:" + iClientReferralId);

                        // Contact Adult Protective Services
                        // Phone Number
                        String phoneNumber = null;

                        // Call the webservice to get the APS phone number
                        JSONObject numbers = callAPS.getPriorityNumbers();

                        // Parse the Data and return an ArrayList of Phone Numbers
                        parser = new JsonParser();
                        ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
                        alPhoneNumbers = parser.getPriorityNumbersFromJson(numbers);

                        // Loop through the phone numbers
                        for (String[] number : alPhoneNumbers) {

                            // Search for APS Contact Information
                            if (number[1].contains("APS")) {
                                phoneNumber = number[2];
                            }
                        }

                        // Create Intent to call phone number
                        Intent phoneCall = new Intent(Intent.ACTION_CALL);
                        phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
                        phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(phoneCall);
                    }
                });

                // Negative Button
                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                });

                // Display Dialog
                alert.show();

                break;
        }
    }


    ////////////////////// METHODS //////////////////////

    private void getQuestionSections() {

        String[] data;

        if (sSessionId == "") {

            // Gather the Data for POSTing
            data = new String[2];
            data[0] = sRegId;
            data[1] = sQuizId;

            // Make Webservice Call to get list of Questions
            Webservices Questions = new Webservices(this);
            JSONObject results = Questions.getQuestionsByQuizId(data);

            // Parse the Data and return an ArrayList of Questions
            JsonParser parser = new JsonParser();
            alQuestionsData = parser.getQuestionsFromJson(results);

            // Update Session ID Variable
            sSessionId = alQuestionsData.get(0)[1];

            String s = alQuestionsData.get(1)[1];
            if (s.contains("true"))
                isSlideshow = true;
            else
                isSlideshow = false;

            sSectionName = alQuestionsData.get(2)[1];
            sScoreMetric = alQuestionsData.get(3)[1];
        } else {

            // Gather the Data for POSTing
            data = new String[3];
            data[0] = sRegId;
            data[1] = sQuizId;
            data[2] = sSessionId;

            // Make Webservice Call to get list of Questions
            Webservices Questions = new Webservices(this);
            JSONObject results = Questions.getQuestionsByQuizId(data);

            // Parse the Data and return an ArrayList of Questions
            JsonParser parser = new JsonParser();
            alQuestionsData = parser.getQuestionsFromJson(results);

            String s = alQuestionsData.get(1)[1];
            if (s.contains("true"))
                isSlideshow = true;
            else
                isSlideshow = false;

            sSectionName = alQuestionsData.get(2)[1];
            sScoreMetric = alQuestionsData.get(3)[1];
        }
    }

    // Build Financial Capacity Quiz
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void buildQuiz() {

        // Assign the Table Layout to the Question Container in the XML
        svQuizScrollView = (ScrollView) findViewById(R.id.quizScrollView);
        TextView tvAneDescription = (TextView) findViewById(R.id.tvAneQuizesDescription);
        // Check for Free/Full
        if (!sType.toLowerCase().contains("cognitive")) {
            // Get Officer Directions and Script Text Widgets
            TextView tvOfficerDirections = (TextView) findViewById(R.id.tvOfficerDirections);
            TextView tvOfficerScript = (TextView) findViewById(R.id.tvOfficerScript);
            TextView tvSpacer4 = (TextView) findViewById(R.id.tvQuizSpacer4);

            // Hide Officer Directions and Script
            tvOfficerDirections.setVisibility(View.GONE);
            tvOfficerScript.setVisibility(View.GONE);
            tvSpacer4.setVisibility(View.GONE);
            tvAneDescription.setText("");
        }

        if (sType.toLowerCase().contains("abuse")) {
            tvAneDescription.setText("Abuse means willful infliction of physical pain, physical injury, sexual abuse, mental anguish, unreasonable confinement, or the willful deprivation of essential services to a vulnerable adult.");
        } else if (sType.toLowerCase().contains("exploitation")) {
            tvAneDescription.setText("Financial exploitation means the illegal or improper use of a disabled adult's or elder person's money or that person's resources through undue influence, coercion, harassment, duress, deception, false representation, false pretense, or other similar means for one's own or another's profit or advantage.");
        } else if (sType.toLowerCase().contains("neglect")) {
            tvAneDescription.setText("Neglect is the absence or omission of essential services to the degree that it harms or threatens with harm the physical or emotional health of a vulnerable adult.");
        }

        // Generate the Questions Layout
        generateQuestionsLayout();

        // Get the Button NoTouch/Touch Graphics
        buttonTouch = getResources().getDrawable(R.drawable.button_touch);
        buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

        // Create the clickable Effects
        StateListDrawable submitButtonEffects = new StateListDrawable();
        submitButtonEffects.addState(new int[]{android.R.attr.state_pressed}, buttonTouch);
        submitButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

        // Submit Button and ClickListener
        btnSubmit = (Button) findViewById(R.id.bSubmitQuiz);
        btnSubmit.setBackground(submitButtonEffects);
        btnSubmit.setOnClickListener(this);

        // Create the clickable Effects
        StateListDrawable referralButtonEffects = new StateListDrawable();
        referralButtonEffects.addState(new int[]{android.R.attr.state_pressed}, buttonTouch);
        referralButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

        // Results TextView
        llResultContainer = (LinearLayout) findViewById(R.id.quizResultContainer);
        tvQuizResults = (TextView) findViewById(R.id.tvQuizResults);
        btnMakeReferral = (Button) findViewById(R.id.bMakeReferral);
        btnMakeReferral.setBackground(referralButtonEffects);
        btnMakeReferral.setOnClickListener(this);
    }

    // Build Financial Capacity Quiz
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void buildQuizSlideshow() {

        TextView tvSectionName = (TextView) findViewById(R.id.tvSectionName);
        TextView tvQuestionSlideshowQuestion = (TextView) findViewById(R.id.tvQuestionSlideshowQuestion);
        TextView tvAnswer = (TextView) findViewById(R.id.tvQuestionSlideshowAnswer);
        TextView tvTimeToAnswer = (TextView) findViewById(R.id.tvQuestionSlideshowTimeToAnswer);
        TextView tvPositiveDescription = (TextView) findViewById(R.id.tvQuestionSlideshowPositiveDescription);
        TextView tvNegativeDescription = (TextView) findViewById(R.id.tvQuestionSlideshowNegativeDescription);

        // Concatenate Question Strings together
        int buffer = 4;
        String[] saQuestion = alQuestionsData.get(buffer + iSlideshowCounter);

        // Set Text
        if (saQuestion[1].length() > 0)
            tvSectionName.setText(saQuestion[1]);
        else
            tvSectionName.setText(sSectionName);

        tvSectionName.setTextSize((float) (20 * iScalar));

        tvQuestionSlideshowQuestion.setText(saQuestion[2]);
        tvQuestionSlideshowQuestion.setTextSize((float) (14 * iScalar));

        if (saQuestion[3].length() > 0) {
            tvAnswer.setVisibility(View.VISIBLE);
            tvAnswer.setText("[Answer:" + saQuestion[3] + "]");
            tvAnswer.setTextSize((float) (12 * iScalar));
        } else
            tvAnswer.setVisibility(View.GONE);

        if (saQuestion[4].length() > 0 && Integer.parseInt(saQuestion[4]) > 0) {
            tvTimeToAnswer.setVisibility(View.VISIBLE);
            tvTimeToAnswer.setText("Allow the person upto " + saQuestion[4] + " seconds to respond");
            tvTimeToAnswer.setTextSize((float) (12 * iScalar));
        } else
            tvTimeToAnswer.setVisibility(View.GONE);

        if (saQuestion[5].length() > 0) {
            tvPositiveDescription.setVisibility(View.VISIBLE);
            tvPositiveDescription.setText(saQuestion[5]);
            tvPositiveDescription.setTextSize((float) (12 * iScalar));
        } else
            tvPositiveDescription.setVisibility(View.GONE);

        if (saQuestion[6].length() > 0) {
            tvNegativeDescription.setVisibility(View.VISIBLE);
            tvNegativeDescription.setText(saQuestion[6]);
            tvNegativeDescription.setTextSize((float) (12 * iScalar));
        } else
            tvNegativeDescription.setVisibility(View.GONE);

        // Set attributes
        //tvQuestionSlideshowQuestion.setPadding(0, 0, 0, 20);
        tvQuestionSlideshowQuestion.setTextColor(Color.BLUE);
        tvQuestionSlideshowQuestion.setSingleLine(false);
        tvQuestionSlideshowQuestion.setEllipsize(TextUtils.TruncateAt.END);
        tvQuestionSlideshowQuestion.setMaxLines(5);

        // Get the Button NoTouch/Touch Graphics
        buttonTouch = getResources().getDrawable(R.drawable.button_touch);
        buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

        // Create the clickable Effects
        StateListDrawable CorrectButtonEffects = new StateListDrawable();
        CorrectButtonEffects.addState(new int[]{android.R.attr.state_pressed}, buttonTouch);
        CorrectButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

        // Submit Button and ClickListener
        btnCorrectAnswer = (Button) findViewById(R.id.btnCorrectAnswer);
        btnCorrectAnswer.setBackground(CorrectButtonEffects);
        btnCorrectAnswer.setText(saQuestion[7]);
        btnCorrectAnswer.setTextSize((float) (14 * iScalar));
        btnCorrectAnswer.setOnClickListener(this);

        // Create the clickable Effects
        StateListDrawable IncorrectButtonEffects = new StateListDrawable();
        IncorrectButtonEffects.addState(new int[]{android.R.attr.state_pressed}, buttonTouch);
        IncorrectButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

        // Results TextView
        btnIncorrectAnswer = (Button) findViewById(R.id.btnIncorrectAnswer);
        btnIncorrectAnswer.setBackground(IncorrectButtonEffects);
        btnIncorrectAnswer.setText(saQuestion[8]);
        btnIncorrectAnswer.setTextSize((float) (14 * iScalar));
        btnIncorrectAnswer.setOnClickListener(this);
    }

    // Build the Questions
    private void buildQuestionsArray() {
        // Set the Questions from the Data Array to the Questions Array
        alQuestions.clear();
        int ct = 0;
        for (String[] item : alQuestionsData) {
            // Bypass the Session Id
            if (ct > 3)
                alQuestions.add(item[1]);

            ct += 1;
        }
    }

    // Generate Questions in Layout
    @SuppressWarnings("deprecation")
    private void generateQuestionsSlideshowLayout() {
        // Create Question object
        Question oQuestion;

        // Create the Questions
        for (int i = 1; i <= alQuestions.size(); i++) {
            // Instantiate new Question Object and add data to it (checkedStatus defaults to false)
            oQuestion = new Question();
            oQuestion.setId(alQuestionsData.get(i)[0]);
            oQuestion.setQuestion(alQuestions.get(i));
            oQuestionContainer.addQuestion(oQuestion);
        }
    }

    // Generate Questions in Layout
    @SuppressWarnings("deprecation")
    private void generateQuestionsLayout() {

        // Get the Question Container
        TableLayout tlQuestionLayout = (TableLayout) findViewById(R.id.quizQuestionContainer);

        // Create Question object
        Question oQuestion;

        // Create the Questions
        for (int i = 4; i < alQuestionsData.size(); i++) {

            // Instantiate new Question Object and add data to it (checkedStatus defaults to false)
            oQuestion = new Question();
            oQuestion.setId(alQuestionsData.get(i)[0]);
            oQuestion.setQuestion(alQuestionsData.get(i)[1]);
            oQuestion.setPointValue(Integer.parseInt(alQuestionsData.get(i)[2]));
            oQuestionContainer.addQuestion(oQuestion);

            // Create the Row
            TableRow trQuestionRow = new TableRow(this);
            trQuestionRow.setLayoutParams(new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT, 1f));

            // Create TextViews with Question
            TextView question = new TextView(this);
            question.setPadding(0, 0, 0, 20);
            question.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, 1f));
			/*if (("Q" + i + ". " + alQuestions.get(i-1)).length() < 70){
				question.setLayoutParams(new TableRow.LayoutParams(0, 80, .9f));
			}
			else if (("Q" + i + ". " + alQuestions.get(i-1)).length() < 110){
				question.setLayoutParams(new TableRow.LayoutParams(0, 100, .9f));
			}
			else if (("Q" + i + ". " + alQuestions.get(i-1)).length() < 150){
				question.setLayoutParams(new TableRow.LayoutParams(0, 130, .9f));
			}
			else if (("Q" + i + ". " + alQuestions.get(i-1)).length() >= 150){
				question.setLayoutParams(new TableRow.LayoutParams(0, 165, .9f));
			}*/

            question.setTextColor(Color.BLUE);
            question.setSingleLine(false);
            question.setEllipsize(TextUtils.TruncateAt.END);
            question.setMaxLines(5);

            // Concatenate Question Strings together
            question.setText("Q" + (i - 3) + ". " + oQuestion.getQuestion());

            // Create the checkbox
            CheckBox checkbox = new CheckBox(this);
            checkbox.setButtonDrawable(R.drawable.checkbox_selector);
            checkbox.setId(i - 3);
            checkbox.setLayoutParams(new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, .1f));
            checkbox.setText("");
            checkbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                                             boolean isChecked) {
                    // Update Quiz score
                    oQuestionContainer.updateQuizScore(isChecked);

                    // Update the checked Status of the Question
                    oQuestionContainer.updateCheckedStatusByQuestionId(buttonView.getId() - 1, isChecked);
                }
            });

            // Add children to Row
            trQuestionRow.addView(question);
            trQuestionRow.addView(checkbox);

            // Add the completed row to the Container
            tlQuestionLayout.addView(trQuestionRow);

            // Create Spacer
            TextView spacer = new TextView(this);

            // Add Spacer to Container
            tlQuestionLayout.addView(spacer);
        }
    }

    // Phone call Intent to Adult Protective Services (APS)
    private void contactAPS() {

        // Instantiate Webservice
        final Webservices callAPS = new Webservices(this);

        // Dialog requesting permission to make Phone call
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Call APS");
        alert.setMessage("Do you want to call Adult Protective Services?");

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Phone Number
                String phoneNumber = null;

                // Call the webservice to get the APS phone number
                JSONObject results = callAPS.getPriorityNumbers();

                // Parse the Data and return an ArrayList of Phone Numbers
                JsonParser parser = new JsonParser();
                ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
                alPhoneNumbers = parser.getPriorityNumbersFromJson(results);

                // Loop through the phone numbers
                for (String[] numbers : alPhoneNumbers) {

                    // Search for APS Contact Information
                    if (numbers[1].contains("APS")) {
                        phoneNumber = numbers[2];
                    }
                }

                // Create Intent to call phone number
                Intent phoneCall = new Intent(Intent.ACTION_CALL);
                phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
                phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(phoneCall);
            }
        });

        // Negative Button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    // Phone call Intent to Alzheimer's Association
    private void contactAlzheimersAssociation() {

        // Instantiate Webservice
        final Webservices callAPS = new Webservices(this);

        // Dialog requesting permission to make Phone call
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Call Alzheimer's Association");
        alert.setMessage("Do you want to call Alzheimer's Association?");

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Phone Number
                String phoneNumber = null;

                // Call the webservice to get the APS phone number
                JSONObject results = callAPS.getPriorityNumbers();

                // Parse the Data and return an ArrayList of Phone Numbers
                JsonParser parser = new JsonParser();
                ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
                alPhoneNumbers = parser.getPriorityNumbersFromJson(results);

                // Loop through the phone numbers
                for (String[] numbers : alPhoneNumbers) {
                    // Search for APS Contact Information
                    if (numbers[1].contains("Association")) {
                        phoneNumber = numbers[2];
                    }
                }

                // Create Intent to call phone number
                Intent phoneCall = new Intent(Intent.ACTION_CALL);
                phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
                phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(phoneCall);
            }
        });

        // Negative Button
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    // Activation is successful. Add the Access code to the DB and activate the App
    final public void activateApp(String accessCode) {

        // Database connection
        final DatabaseHandler db = new DatabaseHandler(this);
        final String access = accessCode;

        // Dialog to display Successful activation
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Activation Success");
        alert.setMessage("App Activated to Personnel Version");

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                // Insert the Access Code into the DB and call onCreate
                if (db.updateUserAccessCode(access)) {

                    // Activate the App to Full Version
                    bIsFullVersion = true;

                    // Close the connection to the DB
                    db.close();

                    // Create Restart App Intent
                    Intent restartApp = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(getBaseContext().getPackageName());

                    // Add Flag and start Intent
                    restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(restartApp);
                }

                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();

    }

    // Display a dialog showing the message for Failing to activate the App
    final public void displayFailedActivationMessage(String message) {

        // Dialog to display failed activation
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Activation Failed");
        alert.setMessage(message);

        // Positive Button
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });

        // Display Dialog
        alert.show();
    }

    //////////////////////////////////////////////////////////////////////////////////////

    // Question Container
    private class QuestionContainer {

        // Container for the Questions
        private ArrayList<Question> alContainer;
        private Question oQuestion;
        private int iQuizScore;

        public QuestionContainer() {
            alContainer = new ArrayList<Question>();
            oQuestion = new Question();
            iQuizScore = 0;
        }

        // Methods
        public ArrayList<Question> getQuestionContainer() {
            return alContainer;
        }

        @SuppressWarnings("unused")
        public Question getQuestionById(int id) {
            return alContainer.get(id);
        }

        @SuppressWarnings("unused")
        public int getQuizScore() {
            return iQuizScore;
        }

        public int getQuizCount() {
            return alContainer.size();
        }

        // Create a question and add Question to container
        @SuppressWarnings("unused")
        public void addQuestion(String id, String question, int point_value) {
            oQuestion = new Question(id, question, point_value, false);
            alContainer.add(oQuestion);
        }

        public Question getQuestionByIndex(int index) {
            return alContainer.get(index);
        }

        public String getQuestionIdByIndex(int index) {
            return alContainer.get(index).getId();
        }

        // Add a Question
        public void addQuestion(Question question) {
            alContainer.add(question);
        }

        // Update the Checked Status of the Question
        public void updateCheckedStatusByQuestionId(int id, boolean checked) {
            alContainer.get(id).setCheckedStatus(checked);
        }

        // Update the Quiz Score
        public void updateQuizScore(boolean value) {
            // Update Quiz score
            if (value)
                iQuizScore += 1;
            else
                iQuizScore -= 1;
        }
    }

    // Question Class
    private class Question {

        // Class Variables
        private String sId;
        private String sQuestion;
        private int iPointValue;
        private boolean isChecked;

        // Default Constructor
        public Question() {
            sId = "0";
            sQuestion = "";
            iPointValue = 1;
            isChecked = false;
        }

        // Constructor
        public Question(String id, String question, int point_value, boolean checked) {
            sId = id;
            sQuestion = question;
            iPointValue = point_value;
            isChecked = checked;
        }

        public String getId() {
            return sId;
        }

        @SuppressWarnings("unused")
        public String getQuestion() {
            return sQuestion;
        }

        public int getPointValue() {
            return iPointValue;
        }

        public boolean getCheckedStatus() {
            return isChecked;
        }

        public void setId(String id) {
            sId = id;
        }

        public void setQuestion(String question) {
            sQuestion = question;
        }

        public void setPointValue(int point_value) {
            iPointValue = point_value;
        }

        public void setCheckedStatus(boolean checked) {
            isChecked = checked;
        }
    }


}


























