package com.eyeon.gane;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.util.Log;
import android.util.StateSet;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
@SuppressLint("NewApi")
public class ReportMissingAdult extends AppCompatActivity implements OnClickListener
{

	// Activity Variables
	boolean bIsFullVersion = false;
	Bundle savedData = new Bundle();
	
	// Layout Widgets
	Spinner sprMattiesCall;
	
	Button bMattiesCall;
	Button bSafeReturn;
	Button bActivateMattiesCall;

	TextView tvMattiesCallDescription;
	TextView tvOrSpacer;
	TextView tvMattiesCallLabel;
	TextView tvSafeReturnLabel;
	
	Spinner sprMattiesCallReason;
	
	// Activation Widget
	Spinner sprUserType = null;
	
	// Graphics
	Drawable buttonTouch;
	Drawable buttonNoTouch;
	
	// Activity Context
	Context mContext;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// Get Savestate and call super
		savedData = savedInstanceState;
		super.onCreate(savedData);
		
		// Get Context
		mContext = this;
		
		// Get Intent and pull extra to determine which screen to draw
		Intent intent = getIntent();
		bIsFullVersion = intent.getBooleanExtra("version", false);
		
		// Set the Layout and Title
		setContentView(R.layout.report_missing_adults);
		setTitle("Report Missing Adult");
		
		// Focus the ScrollView to the bottom
		final ScrollView sv = (ScrollView)findViewById(R.id.rmaScrollView);
		
		// Get the Spinner Widget
		sprMattiesCallReason = (Spinner)findViewById(R.id.spMattiesCall);
		sprMattiesCallReason.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) 
			{
				
				sv.postDelayed(new Runnable() 
				{
				    @Override
				    public void run() 
				    {
				    	sv.fullScroll(ScrollView.FOCUS_DOWN);
				    }
				}, 1);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {}
		});

		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.MattiesCallSpinnerText, R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
		sprMattiesCallReason.setAdapter(adapter);
		
		// Draw the Layout
		drawLayout();
		
		bActivateMattiesCall = (Button) findViewById(R.id.bActivateMattiesCall);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Only display in Free version
		if (!bIsFullVersion)
		{
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.activation, menu);
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Create Dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		// Set title and message
		alert.setTitle("Activate Personnel Version");
		alert.setMessage("Enter Activation Code");
		
		// String Array of the User Types
		String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};
		
		// Create LinearLayout
		LinearLayout activationLayout = new LinearLayout(this);
		activationLayout.setOrientation(LinearLayout.VERTICAL);
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		sprUserType = new Spinner(this);

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sprUserType.setAdapter(spinnerArrayAdapter);
		
		// Add Widgets to Layout
		activationLayout.addView(input);
		activationLayout.addView(sprUserType);

		// Set the Dialog's View
		alert.setView(activationLayout); //input);
		
		// Initialize the Webservice
		final Webservices activationCall = new Webservices(this);
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		final String regId = dbHandler.getRegId();
		dbHandler.close();
		
		// Set positive button - Listener is Null, overriden below
		alert.setPositiveButton("Ok", null);

		// Set negative button
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
		  public void onClick(DialogInterface dialog, int whichButton) 
		  {
			  dialog.dismiss();
		  }
		});
		
		// Create the Alert Dialog
		final AlertDialog activationDialog = alert.create();
		
		// Set the Dialog Show Listener to enforce Validation on the User Role Selection
		activationDialog.setOnShowListener(new DialogInterface.OnShowListener() 
		{
			
			@Override
			public void onShow(DialogInterface dialog) 
			{
				
				// Get the Positive Button
				Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				// Add a Click Listener
				positiveButton.setOnClickListener(new View.OnClickListener() 
				{

		            @Override
		            public void onClick(View view) 
		            {
		                // TODO Do something
		            	// Get the value
						Editable value = input.getText();
						String userRole = sprUserType.getSelectedItem().toString();
						
						if (!userRole.contains("Select"))
						{
							// Create the Array and add the values to the array
							String[] data = new String[3];
							data[0] = value.toString(); 	// Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)
							
							// Get the GCM Registration ID
							data[1] = regId; // GCM Registration ID	  
							
							// Get the UserType
							data[2] = userRole; // User Type (Public, APS, Law Enforcement, Other)
							
						    // Get the results
							JSONObject results = activationCall.postActivationCode(data);

							// Parse the Data and return an ArrayList of Questions
							JsonParser parser = new JsonParser();
							String[] activationResults = parser.checkPostActivationCodeSubmission(results);
							
							if (activationResults.length == 1)
							{
								// Activate the App
								activateApp(data[0]);
							}
							else
							{
								// Activation Failed - Display Fail Message
								displayFailedActivationMessage(activationResults[1]);
							}
			            	
			                // Close the Dialog
			            	activationDialog.dismiss();
						}
						else
						{
							// Display Invalid Data Notification
							Toast.makeText(ReportMissingAdult.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
						}

		            }
		        });
			}
		});

		// Show Dialog
		activationDialog.show();

		// Return True
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);
		
		// Redraw Layout
		drawLayout();
		
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		
		savedInstanceState.putBoolean("version", bIsFullVersion);
		
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		
		bIsFullVersion = savedInstanceState.getBoolean("version", false);
		
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	@Override
	public void onClick(View v) 
	{
		
		// Instantiate Webservice
		final Webservices callPriorityNumbers = new Webservices(this);
		
		switch(v.getId())
		{
		case R.id.bInitiateMattiesCall:
			
			
			// Hide the Or Spacer and the Safe Return Fields
			bMattiesCall.setVisibility(View.GONE);
			tvMattiesCallLabel.setVisibility(View.GONE);
			bSafeReturn.setVisibility(View.GONE);
			tvSafeReturnLabel.setVisibility(View.GONE);
			
			// Update Text
			tvOrSpacer.setText(R.string.InitiateMattiesCallPromptText);
			tvOrSpacer.setGravity(Gravity.LEFT);
			
			// Set Activate Button to visible
			StateListDrawable activateMattiesCallButtonEffects = new StateListDrawable();
			activateMattiesCallButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
			activateMattiesCallButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
			

				bActivateMattiesCall.setOnClickListener(new View.OnClickListener() 
				{
					
					@Override  
					public void onClick(View v) 
					{
						// Validation check on the Reason Selected
						if (sprMattiesCallReason.getSelectedItemPosition() > 0)
						{
							
							// Create Dialog
							AlertDialog.Builder MattiesCall = new AlertDialog.Builder(mContext);

							// Set Title and Message
							MattiesCall.setTitle("Mattie's Call");
							MattiesCall.setMessage("Do you want to call Mattie's Call");

							// Webservice, DatabaseHandler and JsonParser classes
							final Webservices mattiesCallWebservice = new Webservices(mContext);
							final DatabaseHandler db = new DatabaseHandler(mContext);
							final JsonParser parser = new JsonParser();
							
							// Set positive button
							MattiesCall.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog, int whichButton) 
								{
								
									// Phone Number
									String phoneNumber = null;
									
									// Call the webservice to get the Mattie's Call phone number
									JSONObject results = callPriorityNumbers.getPriorityNumbers();
									
									// Parse the Data and return an ArrayList of Phone Numbers
									ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
									alPhoneNumbers = parser.getPriorityNumbersFromJson(results);
									
									// Loop through the phone numbers
									for (String[] numbers : alPhoneNumbers)
									{
										
										// Search for Mattie's Call Contact Information
										if (numbers[1].contains("MATTIE"))
										{
											phoneNumber = numbers[2];
										}
									}
									
									// Data ArrayList
									ArrayList<String> data = new ArrayList<String>();
									
									// Access DB for APID
									String apid = db.getRegId();
									
									// Add Data to Array
									data.add(apid);
									data.add(sprMattiesCallReason.getSelectedItem().toString());

									// Webservice Call to store Matties Call Information
									JSONObject jsonResult = mattiesCallWebservice.postMattiesCallInfo(data);
									boolean bMattiesCallSubmissionResults = parser.getMattiesCallSubmissionResultsFromJson(jsonResult);
									
									// Log if Submission fails
									if (!bMattiesCallSubmissionResults)
										Log.e("Matties Call Data Submission", "Submission to Server has failed");

									Webservices webservices = new Webservices(mContext);
									MyPhoneListener phoneListener = new MyPhoneListener(getApplicationContext(),apid);
									TelephonyManager telephonyManager =
											(TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
									// receive notifications of telephony state changes
									telephonyManager.listen(phoneListener, PhoneStateListener.LISTEN_CALL_STATE);
									// Create Intent to call phone number
									Intent phoneCall = new Intent(Intent.ACTION_CALL);
									phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
									phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(phoneCall);
								}
							});

							// Set negative button
							MattiesCall.setNegativeButton("No", new DialogInterface.OnClickListener() 
							{
								  public void onClick(DialogInterface dialog, int whichButton) 
								  {
									  dialog.dismiss();
								  }
							});

							// Show Dialog
							MattiesCall.show();
						}
						else
						{
							Toast.makeText(mContext, "Please choose a Condition", Toast.LENGTH_SHORT).show();
						}
					}
				});
				
				// Set Mattie's Call Activation Widgets to Visible
				sprMattiesCall.setVisibility(View.VISIBLE);
				bActivateMattiesCall.setVisibility(View.VISIBLE);

			
			// Focus the ScrollView to the bottom
			final ScrollView sv = (ScrollView)findViewById(R.id.rmaScrollView);
			sv.postDelayed(new Runnable() 
			{
			    @Override
			    public void run() 
			    {
			    	sv.fullScroll(ScrollView.FOCUS_DOWN);
			    }
			}, 1);
			
			break;
		case R.id.bCallSafeReturn:
			
			// Create Dialog
			AlertDialog.Builder SafeAlert = new AlertDialog.Builder(this);

			// Set Title and Message
			SafeAlert.setTitle("Safe Return");
			SafeAlert.setMessage("Is the individual wearing a Safe Return bracelet/necklace");

			// Set positive button
			SafeAlert.setPositiveButton("Yes", new DialogInterface.OnClickListener() 
			{
				public void onClick(DialogInterface dialog, int whichButton) 
				{
				
					// Phone Number
					String phoneNumber = null;
					
					// Call the webservice to get the Safe Return phone number
					JSONObject results = callPriorityNumbers.getPriorityNumbers();
					
					// Parse the Data and return an ArrayList of Phone Numbers
					JsonParser parser = new JsonParser();
					ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
					alPhoneNumbers = parser.getPriorityNumbersFromJson(results);
					
					// Loop through the phone numbers
					for (String[] numbers : alPhoneNumbers)
					{
						
						// Search for Safe Return Contact Information
						if (numbers[1].contains("SAFERETURN"))
						{
							phoneNumber = numbers[2];
						}
					}
			
					// Create Intent to call phone number
					Intent phoneCall = new Intent(Intent.ACTION_CALL);
					phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
					phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					startActivity(phoneCall);
				}
			});

			// Set negative button
			SafeAlert.setNegativeButton("No", new DialogInterface.OnClickListener() 
			{
				  public void onClick(DialogInterface dialog, int whichButton) 
				  {
					  dialog.dismiss();
					  if(bIsFullVersion){
						  Intent iEmergencyPlacement = new Intent(ReportMissingAdult.this, EmergencyPlacement.class);
						  startActivity(iEmergencyPlacement);
					  }
					  else{
						  AlertDialog.Builder DenySafeAlert = new AlertDialog.Builder(ReportMissingAdult.this);
						  
						  DenySafeAlert.setTitle("Call 911");
						  DenySafeAlert.setMessage("Please call 911 and inform them that there is an adult who appears lost and confused and that you need an officer to come out and assess the situation.");

							// Set positive button
						  DenySafeAlert.setPositiveButton("Call 911", new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog, int whichButton) 
								{
								
									// Phone Number
									String phoneNumber = "911";
									
									
							
									// Create Intent to call phone number
									Intent phoneCall = new Intent(Intent.ACTION_CALL);
									phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
									phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(phoneCall);
								}
							});
						  
						  DenySafeAlert.setNegativeButton("No", new DialogInterface.OnClickListener() 
							{
								public void onClick(DialogInterface dialog, int whichButton) 
								{
								
									// Phone Number
									dialog.dismiss();
								}
							});
						  
						  DenySafeAlert.show();

					  }
				  }
			});

			// Show Dialog
			SafeAlert.show();
			
			break;
		}
	}
	
	////////////////////// METHODS //////////////////////
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	// Show/Hide Layout widgets based on version
	@SuppressLint("NewApi")
	private void drawLayout() 
	{
		
		// Get the Button NoTouch/Touch Graphics
		buttonTouch = getResources().getDrawable(R.drawable.button_touch);
		buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

		// Show/Hide Widgets based on version
		if (bIsFullVersion)
		{

			// Get Mattie's Call Button Widget
			bMattiesCall = (Button)findViewById(R.id.bInitiateMattiesCall);
			
			// Check to see if Matties Call has already been initiated
			if (bMattiesCall.getVisibility() == View.GONE)
			{
				// Refire the Matties Call
				bMattiesCall.callOnClick();
			}
			else
			{
				tvMattiesCallLabel = (TextView)findViewById(R.id.tvMattiesCallRequestLabel);
				tvMattiesCallLabel.setVisibility(View.VISIBLE);
				
				// Create the clickable Effects
				StateListDrawable mattiesCallButtonEffects = new StateListDrawable();
				mattiesCallButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
				mattiesCallButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
				
				bMattiesCall.setBackground(mattiesCallButtonEffects);
				bMattiesCall.setVisibility(View.VISIBLE);
				bMattiesCall.setOnClickListener(this);
				
				tvOrSpacer = (TextView)findViewById(R.id.tvOrSpacer);
				tvOrSpacer.setVisibility(View.VISIBLE);
				
				tvSafeReturnLabel = (TextView)findViewById(R.id.tvSafeReturnRequestLabel);
				tvSafeReturnLabel.setVisibility(View.VISIBLE);
				
				// Create the clickable Effects
				StateListDrawable safeReturnButtonEffects = new StateListDrawable();
				safeReturnButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
				safeReturnButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
				
				bSafeReturn = (Button)findViewById(R.id.bCallSafeReturn);
				bSafeReturn.setBackground(safeReturnButtonEffects);
				bSafeReturn.setOnClickListener(this);
				bSafeReturn.setVisibility(View.VISIBLE);		
				
				// Create the clickable Effects
				//StateListDrawable activateMattiesCallButtonEffects = new StateListDrawable();
				//activateMattiesCallButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
				//activateMattiesCallButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);

				//bActivateMattiesCall = (Button)findViewById(R.id.bActivateMattiesCall);
				
				//bActivateMattiesCall.setVisibility(View.GONE);

				tvOrSpacer = (TextView)findViewById(R.id.tvOrSpacer);
				tvOrSpacer.setVisibility(View.VISIBLE);
				tvSafeReturnLabel = (TextView)findViewById(R.id.tvSafeReturnRequestLabel);
				tvSafeReturnLabel.setVisibility(View.VISIBLE);

				sprMattiesCall = (Spinner)findViewById(R.id.spMattiesCall);
				sprMattiesCall.setVisibility(View.GONE);
			}
		}
		else if (!bIsFullVersion)
		{
			tvMattiesCallDescription = (TextView)findViewById(R.id.tvMattiesCallDescription);
			tvMattiesCallDescription.setVisibility(View.GONE);

			tvMattiesCallLabel = (TextView)findViewById(R.id.tvMattiesCallRequestLabel);
			tvMattiesCallLabel.setVisibility(View.GONE);
			
			bMattiesCall = (Button)findViewById(R.id.bInitiateMattiesCall);
			bMattiesCall.setVisibility(View.GONE);
			
			tvOrSpacer = (TextView)findViewById(R.id.tvOrSpacer);
			tvOrSpacer.setVisibility(View.GONE);
			
			tvSafeReturnLabel = (TextView)findViewById(R.id.tvSafeReturnRequestLabel);
			tvSafeReturnLabel.setVisibility(View.VISIBLE);
			
			// Create the clickable Effects
			StateListDrawable safeReturnButtonEffects = new StateListDrawable();
			safeReturnButtonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
			safeReturnButtonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
			
			bSafeReturn = (Button)findViewById(R.id.bCallSafeReturn);
			bSafeReturn.setBackground(safeReturnButtonEffects);
			bSafeReturn.setOnClickListener(this);
			bSafeReturn.setVisibility(View.VISIBLE);
		}
	}
	
	// Activation is successful. Add the Access code to the DB and activate the App
	final public void activateApp(String accessCode)
	{
		
		// Database connection
		final DatabaseHandler db = new DatabaseHandler(this);
		final String access = accessCode;
		
		// Dialog to display Successful activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Success");
		alert.setMessage("App Activated to Personnel Version");

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				
				// Insert the Access Code into the DB and call onCreate
				if (db.updateUserAccessCode(access))
				{
					
					// Activate the App to Full Version
					bIsFullVersion = true;
					
					// Close the connection to the DB
					db.close();

					// Create Restart App Intent
					Intent restartApp = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(getBaseContext().getPackageName());
					
					// Add Flag and start Intent
					restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(restartApp);
				}
				
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();

	}
	
	// Display a dialog showing the message for Failing to activate the App
	final public void displayFailedActivationMessage(String message)
	{
		
		// Dialog to display failed activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Failed");
		alert.setMessage(message);

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();
	}
	private class MyPhoneListener extends PhoneStateListener {

		private boolean onCall = false;
		private Context ws;
		private String id;

		public MyPhoneListener(Context context, String appId){
			this.ws = context;
			this.id = appId;
		}

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			switch (state) {

				case TelephonyManager.CALL_STATE_OFFHOOK:
					// one call exists that is dialing, active, or on hold
					try {
						Webservices webservices = new Webservices(getApplicationContext());
						JSONObject json=	webservices.postContactUs(id+"_phone_ringing");
						Log.e("Json_phone",json.toString());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					Log.e("Phone","active/hold");
					//because user answers the incoming call
					onCall = true;
					break;

				case TelephonyManager.CALL_STATE_IDLE:
					// in initialization of the class and at the end of phone call

					// detect flag from CALL_STATE_OFFHOOK
					if (onCall == true) {

						Log.e("Phone","restart");
						// restart our application
						Intent restart = getBaseContext().getPackageManager().
								getLaunchIntentForPackage(getBaseContext().getPackageName());
						restart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(restart);

						onCall = false;
					}
					break;
				default:
					break;
			}
		}
	}
}
