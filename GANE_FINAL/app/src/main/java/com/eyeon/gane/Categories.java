package com.eyeon.gane;

import java.util.ArrayList;

import org.json.JSONObject;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.util.StateSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Categories extends AppCompatActivity implements OnClickListener
{

	// Activity Variables
	String sScreen = "";
	Boolean bIsFullVersion = false;
	Bundle savedData = new Bundle();
	Context mContext;
	
	// Server Data
	ArrayList<String[]> alCategoriesData;
	
	// Activation Validation
	Spinner sprUserType = null;
	
	// Graphics
	Drawable buttonTouch;
	Drawable buttonNoTouch;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		savedData = savedInstanceState;
		
		// Get this Activity
		mContext = this;
		
		super.onCreate(savedData);
		
		// Get Intent and pull extra to determine which screen to draw
		Intent intent = getIntent();
		sScreen = intent.getStringExtra("screen");
		bIsFullVersion = intent.getBooleanExtra("version", false);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		
		// Only display in Free Version
		if (!bIsFullVersion)
		{
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.activation, menu);
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Create Dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		// Set title and message
		alert.setTitle("Activate Personnel Version");
		alert.setMessage("Enter Activation Code");
		
		// String Array of the User Types
		String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};
		
		// Create LinearLayout
		LinearLayout activationLayout = new LinearLayout(this);
		activationLayout.setOrientation(LinearLayout.VERTICAL);
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		sprUserType = new Spinner(this);

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sprUserType.setAdapter(spinnerArrayAdapter);
		
		// Add Widgets to Layout
		activationLayout.addView(input);
		activationLayout.addView(sprUserType);

		// Set the Dialog's View
		alert.setView(activationLayout); //input);
		
		// Initialize the Webservice
		final Webservices activationCall = new Webservices(this);
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		final String regId = dbHandler.getRegId();
		dbHandler.close();
		
		// Set positive button - Listener is Null, overriden below
		alert.setPositiveButton("Ok", null);

		// Set negative button
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
		  public void onClick(DialogInterface dialog, int whichButton) 
		  {
			  dialog.dismiss();
		  }
		});
		
		// Create the Alert Dialog
		final AlertDialog activationDialog = alert.create();
		
		// Set the Dialog Show Listener to enforce Validation on the User Role Selection
		activationDialog.setOnShowListener(new DialogInterface.OnShowListener() 
		{
			
			@Override
			public void onShow(DialogInterface dialog) 
			{
				
				// Get the Positive Button
				Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				// Add a Click Listener
				positiveButton.setOnClickListener(new View.OnClickListener() 
				{

		            @Override
		            public void onClick(View view) 
		            {
		                // TODO Do something
		            	// Get the value
						Editable value = input.getText();
						String userRole = sprUserType.getSelectedItem().toString();
						
						if (!userRole.contains("Select"))
						{
							// Create the Array and add the values to the array
							String[] data = new String[3];
							data[0] = value.toString(); 	// Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)
							
							// Get the GCM Registration ID
							data[1] = regId; // GCM Registration ID	  
							
							// Get the UserType
							data[2] = userRole;
							
						    // Get the results
							JSONObject results = activationCall.postActivationCode(data);

							// Parse the Data and return an ArrayList of Questions
							JsonParser parser = new JsonParser();
							String[] activationResults = parser.checkPostActivationCodeSubmission(results);
							
							if (activationResults.length == 1)
							{
								// Activate the App
								activateApp(data[0]);
							}
							else
							{
								// Activation Failed - Display Fail Message
								displayFailedActivationMessage(activationResults[1]);
							}
			            	
			                // Close the Dialog
			            	activationDialog.dismiss();
						}
						else
						{
							// Display Invalid Data Notification
							Toast.makeText(Categories.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
						}

		            }
		        });
			}
		});

		// Show Dialog
		activationDialog.show();

		// Return True
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);
		
		// Load Resources
		loadResources();
		
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		
		savedInstanceState.putString("screen", sScreen);
		savedInstanceState.putBoolean("version", bIsFullVersion);
		
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		
		sScreen = savedInstanceState.getString("screen");
		bIsFullVersion = savedInstanceState.getBoolean("version");
		
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	public void onBackPressed()
	{
		
		if (bIsFullVersion && sScreen.contains("ane"))
		{
			sScreen = "screenings";
			onResume();
		}
		else
			super.onBackPressed();
	}
	
	////////////////////// METHODS ////////////////////////////////////
	
	// Make Webservice Calls, Fill Arrays and Draw Layout
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	private void loadResources()
	{
		
		// Initialize ArrayLists
		alCategoriesData = new ArrayList<String[]>();
		
		// Get the Button NoTouch/Touch Graphics
		buttonTouch = getResources().getDrawable(R.drawable.button_touch);
		buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);

		// Check the Screen extra and set the content view to the respective choice
		if (sScreen.contains("agency"))
		{

			////////////////////// AGENCY CATEGORY LAYOUT //////////////////////
			
			// Make Webservice Call to get list of Agency Categories
			Webservices Agencies = new Webservices(this);
			JSONObject results = Agencies.getAgencyCategories();

			// Parse the Data and return an ArrayList of Questions
			JsonParser parser = new JsonParser();
			alCategoriesData = parser.getAgencyCategoriesFromJson(results);
			
			// Set the Layout View and Title
			setContentView(R.layout.agency);
			setTitle("Agency");

			// Get Layout widget
			LinearLayout agencyLayout = (LinearLayout)findViewById(R.id.agencyLinearLayout);

			// Loop through the Category Array locating the respective Agency Types to get their Names
			for (final String[] item : alCategoriesData)
			{			
				
				// Get Button, Set Background and attributes
				Button agencyCat = new Button(this);
				agencyCat.setBackground(getResources().getDrawable(R.drawable.laws_button));
				agencyCat.setText(item[1]);
				
				// Set the Layout Parameters
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 8;
				params.leftMargin = 15;
				params.rightMargin = 15;
				agencyCat.setLayoutParams(params);
				
				// Set Click Listener
				agencyCat.setOnClickListener(new OnClickListener()
				{

					@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
					@Override
					public void onClick(View v) 
					{
						// {id, name}
						goToAgencyList(item[0], item[1]);
					}
				});
				
				// Add Button to Layout
				agencyLayout.addView(agencyCat);

			}
		}
		else if (sScreen.contains("screenings"))
		{

			////////////////////// QUIZ CATEGORY LAYOUT //////////////////////
			
			// Make Webservice Call to get list of Quiz Categories
			Webservices Quizzes = new Webservices(this);
			JSONObject results = Quizzes.getQuizCategories();
			
			// Parse the Data and return an ArrayList of Quiz Categories
			JsonParser parser = new JsonParser();
			alCategoriesData = parser.getQuizzesFromJson(results);
			
			// Set the Layout and Title
			setContentView(R.layout.screenings);
			
			// Set Title respectively
			if (sScreen.contains("ane"))
				setTitle("Screenings ANE");
			else
				setTitle("Screenings");
			
			// Get Screenings Layout widget
			LinearLayout screeningLayout = (LinearLayout)findViewById(R.id.screeningLinearLayout);

			// Loop through the Category Array locating the respective Quiz Types to get their Names
			for (final String[] item : alCategoriesData)
			{
				
				// Get Button and set attributes
				Button screenCat = new Button(this);
				// Create the layout params for the Buttons
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 8;
				params.leftMargin = 15;
				params.rightMargin = 15;
				screenCat.setLayoutParams(params);
				screenCat.setBackground(getResources().getDrawable(R.drawable.laws_button));
				
				// Set Click Listener
				screenCat.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v) 
					{
						// {id, quiz_type, get_client_info}
						goToQuiz(item[0], item[1], item[3]);
					}
				});
				
				// Check for Screening Type
				if (!sScreen.contains("ane"))
				{
					if (bIsFullVersion && !(item[1].contains("Abuse") || item[1].contains("Neglect") || item[1].contains("Exploitation")))
					{
						screenCat.setText(item[2]);
						
						// Add Button to Layout
						screeningLayout.addView(screenCat);
					}
				}
				else
				{
					if (item[1].contains("Abuse") || item[1].contains("Neglect") || item[1].contains("Exploitation"))
					{
						screenCat.setText(item[2]);
						
						// Add Button to Layout
						screeningLayout.addView(screenCat);
						TextView screeningText = (TextView) findViewById(R.id.screeningsText);
						screeningText.setText(R.string.mainScreeningText);
					}
				}
			}

			// Check if Full Version and if view is Screenings - add Screenings ANE Button
			if (bIsFullVersion && !sScreen.contains("ane"))
			{

				// Get Button and set attributes
				Button screenANE = new Button(this);
				// Create the layout params for the Buttons
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 8;
				params.leftMargin = 15;
				params.rightMargin = 15;
				screenANE.setLayoutParams(params);
				screenANE.setBackground(getResources().getDrawable(R.drawable.laws_button));
				screenANE.setText("A/N/E Screening");
				screenANE.setOnClickListener(new View.OnClickListener() 
				{
					@Override
					public void onClick(View v) 
					{
						
						// Update Quiz Category Flag
						sScreen = "screenings_ane";
						
						// Reload the Data and display ANE Quizzes
						loadResources();
					}
				});
				
				// Add the Button to the Layout
				screeningLayout.addView(screenANE);
			}
		}
		else if (sScreen.contains("laws"))
		{
			
			////////////////////// LAW CATEGORY LAYOUT //////////////////////
			
			// Make Webservice Call to get list of Law Categories
			Webservices Laws = new Webservices(this);
			JSONObject results = Laws.getLawCategories();
			
			// Parse the Data and return an ArrayList of Categories
			JsonParser parser = new JsonParser();
			alCategoriesData = parser.getLawCategoriesFromJson(results);

			// Set ContentView and Page Title
			setContentView(R.layout.laws);
			setTitle("Laws");
			
			// Get Layout
			LinearLayout lawLayout = (LinearLayout)findViewById(R.id.lawLinearLayout);

			// Loop through the Category Array locating the respective Agency Types to get their Names
			for (final String[] item : alCategoriesData)
			{			

				// Get Button and set attributes
				Button lawCat = new Button(this);

				// Set the Layout Parameters
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
				params.topMargin = 8;
				params.leftMargin = 15;
				params.rightMargin = 15;
				lawCat.setLayoutParams(params);
				lawCat.setText(item[1]);
				lawCat.setBackground(getResources().getDrawable(R.drawable.laws_button));

				// Set Click Listener
				lawCat.setOnClickListener(new OnClickListener()
				{

					@Override
					public void onClick(View v) 
					{
						// {id, name}
						goToLawList(item[0], item[1]);
					}
				});
				
				// Add Button to Layout
				lawLayout.addView(lawCat);
			}
			
			// Set ClickListener on NexisLexis Disclamer text
			TextView tvNexisLexis = (TextView)findViewById(R.id.tvLawsLexisNexus);
			tvNexisLexis.setOnClickListener(this);
		}
	}

	// Intent Call to Agency List and pass extras
	private void goToAgencyList(String catId, String category)
	{
		Intent iAgency = new Intent(this, ListItemsView.class);
		iAgency.putExtra("screen", "agency");
		iAgency.putExtra("category", category);
		iAgency.putExtra("category_id", catId);
		iAgency.putExtra("version", bIsFullVersion);
		startActivity(iAgency);
	}
	
	// Intent Call to Law List and pass extras
	private void goToLawList(String catId, String category)
	{
		Intent iLaw = new Intent(this, ListItemsView.class);
		iLaw.putExtra("screen", "law");
		iLaw.putExtra("category", category);
		iLaw.putExtra("category_id", catId);
		iLaw.putExtra("version", bIsFullVersion);
		startActivity(iLaw);
	}
	
	// Intent Call to Quiz and pass extras
	private void goToQuiz(String id, String type, String getClientInfo)
	{
		Intent iQuiz = new Intent(this, Quiz.class);
		iQuiz.putExtra("type", type);
		iQuiz.putExtra("version", bIsFullVersion);
		iQuiz.putExtra("quiz_id", id);
		iQuiz.putExtra("get_client_info", getClientInfo);
		startActivity(iQuiz);
	}
	
	// Activation is successful. Add the Access code to the DB and activate the App
	final public void activateApp(String accessCode)
	{
		
		// Database connection
		final DatabaseHandler db = new DatabaseHandler(this);
		final String access = accessCode;
		
		// Dialog to display Successful activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Success");
		alert.setMessage("App Activated to Personnel Version");

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				
				// Insert the Access Code into the DB and call onCreate
				if (db.updateUserAccessCode(access))
				{
					
					// Activate the App to Full Version
					bIsFullVersion = true;
					
					// Close the connection to the DB
					db.close();

					// Create Restart App Intent
					Intent restartApp = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(getBaseContext().getPackageName());
					
					// Add Flag and start Intent
					restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(restartApp);
				}
				
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();

	}
	
	// Display a dialog showing the message for Failing to activate the App
	final public void displayFailedActivationMessage(String message)
	{
		
		// Dialog to display failed activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Failed");
		alert.setMessage(message);

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();
	}

	@Override
	public void onClick(View v) 
	{
		
		switch(v.getId())
		{
		case R.id.tvLawsLexisNexus:
			/*
			// Go to Webpage
			Intent iAgency = new Intent(this, URLWebViewActivity.class);
			//iAgency.putExtra("url", "http://www.lexisnexis.com/en-us/about-us/about-us.page");
			iAgency.putExtra("url", "http://www.lexisnexis.com/hottopics/gacode/");
			
			iAgency.putExtra("is_terf", false);
			startActivity(iAgency);
			 */
			
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.lexisnexis.com/hottopics/gacode/"));
			startActivity(browserIntent);
			break;
		
		}
		
	}
}
