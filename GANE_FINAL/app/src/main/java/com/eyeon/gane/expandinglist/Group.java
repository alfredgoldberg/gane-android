package com.eyeon.gane.expandinglist;

import java.util.ArrayList;
import java.util.List;

public class Group 
{

	// Class Variables
	public String value;
	public final List<String> children = new ArrayList<String>();
	
	// Constructor
	public Group(String val)
	{
		this.value = val;
	}
}
