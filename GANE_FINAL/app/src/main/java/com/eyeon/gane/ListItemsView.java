package com.eyeon.gane;

import java.util.ArrayList;

import org.json.JSONObject;

import com.eyeon.gane.expandinglist.Group;
import com.eyeon.gane.expandinglist.MyExpandableListAdapter;
import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;
import com.eyeon.gane.utils.MyLocationListener;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class ListItemsView extends AppCompatActivity implements OnItemClickListener
{

	// Activity Variables
	Bundle savedData = new Bundle();
	String sScreen = "";
	String sCategory = "";
	String sCategoryId = "";
	String sZipCode = "";
	boolean bIsFullVersion = false;
	double dLatitude;
	double dLongitude;
	
	// Activation Widget
	Spinner sprUserType = null;
	
	// ListView Variables
	ListView lv;
	SparseArray<Group> groups = new SparseArray<Group>();

	// ArrayLists of the Data
	ArrayList<String[]> alListingsData;
	ArrayList<String> alListings;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		
		// Store a copy of the instanceState and call super
		savedData = savedInstanceState;
		super.onCreate(savedData);
		
		// Get Intent and pull extra to determine which screen to draw
		Intent intent = getIntent();
		sScreen = intent.getStringExtra("screen");
		sCategory = intent.getStringExtra("category");
		sCategoryId = intent.getStringExtra("category_id");	
		bIsFullVersion = intent.getBooleanExtra("version", false);
		
		// For Emergency Placement
		if (sCategoryId.contains("emergency_placement"))
			sZipCode = intent.getStringExtra("zip_code");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		
		// Only display in Free Version
		if (!bIsFullVersion)
		{
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.activation, menu);
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// Create Dialog
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		// Set title and message
		alert.setTitle("Activate Personnel Version");
		alert.setMessage("Enter Activation Code");
		
		// String Array of the User Types
		String[] userTypes = {"Select User Role", "APS", "Law Enforcement", "Other"};
		
		// Create LinearLayout
		LinearLayout activationLayout = new LinearLayout(this);
		activationLayout.setOrientation(LinearLayout.VERTICAL);
		
		// Set an EditText view to get user input 
		final EditText input = new EditText(this);
		input.setRawInputType(InputType.TYPE_CLASS_NUMBER);
		sprUserType = new Spinner(this);

		ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, userTypes);
		spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sprUserType.setAdapter(spinnerArrayAdapter);
		
		// Add Widgets to Layout
		activationLayout.addView(input);
		activationLayout.addView(sprUserType);

		// Set the Dialog's View
		alert.setView(activationLayout); //input);
		
		// Initialize the Webservice
		final Webservices activationCall = new Webservices(this);
		DatabaseHandler dbHandler = new DatabaseHandler(this);
		final String regId = dbHandler.getRegId();
		dbHandler.close();
		
		// Set positive button - Listener is Null, overriden below
		alert.setPositiveButton("Ok", null);

		// Set negative button
		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
		{
		  public void onClick(DialogInterface dialog, int whichButton) 
		  {
			  dialog.dismiss();
		  }
		});
		
		// Create the Alert Dialog
		final AlertDialog activationDialog = alert.create();
		
		// Set the Dialog Show Listener to enforce Validation on the User Role Selection
		activationDialog.setOnShowListener(new DialogInterface.OnShowListener() 
		{
			
			@Override
			public void onShow(DialogInterface dialog) 
			{
				
				// Get the Positive Button
				Button positiveButton = activationDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				// Add a Click Listener
				positiveButton.setOnClickListener(new View.OnClickListener() 
				{

		            @Override
		            public void onClick(View view) 
		            {
		                // TODO Do something
		            	// Get the value
						Editable value = input.getText();
						String userRole = sprUserType.getSelectedItem().toString();
						
						if (!userRole.contains("Select"))
						{
							// Create the Array and add the values to the array
							String[] data = new String[3];
							data[0] = value.toString(); 	// Activation Code (Valid: 946666 / Expired: 197932 / Invalid: 361230)
							
							// Get the GCM Registration ID
							data[1] = regId; // GCM Registration ID	  
							
							// Get the UserType
							data[2] = userRole;
							
						    // Get the results
							JSONObject results = activationCall.postActivationCode(data);

							// Parse the Data and return an ArrayList of Questions
							JsonParser parser = new JsonParser();
							String[] activationResults = parser.checkPostActivationCodeSubmission(results);
							
							if (activationResults.length == 1)
							{
								// Activate the App
								activateApp(data[0]);
							}
							else
							{
								// Activation Failed - Display Fail Message
								displayFailedActivationMessage(activationResults[1]);
							}
			            	
			                // Close the Dialog
			            	activationDialog.dismiss();
						}
						else
						{
							// Display Invalid Data Notification
							Toast.makeText(ListItemsView.this, "Please Select a User Role", Toast.LENGTH_LONG).show();
						}

		            }
		        });
			}
		});

		// Show Dialog
		activationDialog.show();

		// Return True
		return true;
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);
		
		// Initialize ArrayLists
		alListingsData = new ArrayList<String[]>();
		alListings = new ArrayList<String>();
		
		// Check for screen to draw
		// Agency has a SingleItem ListView
		// Laws has an Expanding ListView
		if (sScreen.contains("agency"))
		{
			// Set Content view and Grab the ListView
			setContentView(R.layout.agency_list);
			lv = (ListView)findViewById(android.R.id.list);
			lv.setOnItemClickListener(this);

			// Get the Phone's Geo Coordinates
			getGeoCoords();

			// Get the ListView items from the webservice call here and load the array
			Webservices Agencies = new Webservices(this);
			
			// Build POST Data
			ArrayList<String> alAgencies = new ArrayList<String>();
			
			// No Category Id is passed if its for Emergency Placement
			if (!sCategoryId.contains("emergency_placement"))
				alAgencies.add(sCategoryId);
			
			// Add the Geo Coordinates to the data
			alAgencies.add("" + dLatitude);
			alAgencies.add("" + dLongitude);
			
			// Pass the ZipCode if the Category Id is Emergency Placement
			if (sCategoryId.contains("emergency_placement"))
				alAgencies.add(sZipCode);
			
			// JSON Results Object
			JSONObject results;
			
			// Get POST Results
			results = Agencies.getAgenciesByCategory(alAgencies);
			
			// Parse the Data and return an ArrayList of Questions
			JsonParser parser = new JsonParser();
			alListingsData = parser.getAgenciesByCategoryFromJson(results);
			
			// Load the Categories Array
			loadAgenciesArray();

			// Create the adapter and set the data source
			ArrayAdapter<String> agencyListViewAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, alListings)
			{
				
				@Override
		        public View getView(int position, View convertView, ViewGroup parent) 
				{
					// Get the View
		            View view =super.getView(position, convertView, parent);

		            // Grab its TextView
		            TextView textView=(TextView) view.findViewById(android.R.id.text1);

		            // Set the Color to White
		            textView.setTextColor(Color.WHITE);

		            // Return the View
		            return view;
		        }
			};
		    
			lv.setAdapter(agencyListViewAdapter);
			
			// Set page Title
			if (sCategoryId.contains("emergency_placement"))
				setTitle("Emergency Placement - Agencies");
			else
				setTitle(sCategory.substring(0, sCategory.length() - 1) + "ies");
			
		}
		else if (sScreen.contains("law"))
		{
			// Set the Content View
			setContentView(R.layout.law_list);
			
			// Get the ListView items from the webservice call here and load the array
			Webservices Laws = new Webservices(this);
			//String[] lawListings = {"category_id", sCategoryId};
			JSONObject results = Laws.getLawsByCategory(sCategoryId);
			
			// Parse the Data and return an ArrayList of Questions
			JsonParser parser = new JsonParser();
			alListingsData = parser.getLawsByCategoryFromJson(results);
			
			// Load the Categories Array
			loadLawsArray();
			
			// Create Expandable ListView	
			createData();
			ExpandableListView listView = (ExpandableListView)findViewById(android.R.id.list);
			MyExpandableListAdapter adapter = new MyExpandableListAdapter(this, groups);
			listView.setAdapter(adapter);

			// Set the Title
			setTitle(sCategory);
		}
		
		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		
		savedInstanceState.putString("category", sCategory);
		savedInstanceState.putString("screen", sScreen);
		savedInstanceState.putBoolean("version", bIsFullVersion);
		
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		
		sCategory = savedInstanceState.getString("category");
		sScreen = savedInstanceState.getString("screen");
		bIsFullVersion = savedInstanceState.getBoolean("version", false);
		
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) 
	{
		
		// Check for layout
		if (sScreen.contains("agency"))
		{
			
			// Use position to get Agency Information
			String[] agency = alListingsData.get(position);
			String agencyId = agency[0];
			
			// Call Intent to the Agency Details Page
			Intent iAgency = new Intent(this, ListItemDetails.class);
			iAgency.putExtra("version", bIsFullVersion);
			iAgency.putExtra("agency_id", agencyId);
			
			startActivity(iAgency);
		}
	}
	
	private void getGeoCoords()
	{
		
		// Get GeoCoords
		MyLocationListener listener = new MyLocationListener(this);
		
		if (listener.canGetLocation())
		{
			dLatitude = listener.getLatitude();
			dLongitude = listener.getLongitude();
		}
		else
		{
			// Prompt User for GEO Location Permissions
			listener.showSettingsAlert();
		}
	}
	
	// Create the Groups for the Law's ExpandedView page
	public void createData() 
	{
		
		// Create a newline and count variable
		String newline = "\n";
		int count = 0;
		
		// Loop through Data Array
		for (String[] item : alListingsData)
		{
			// Create the Listing's Header Text
			Group group;
			
			// Check the length of the description text
			//if (item[2].length() <= 35){
				//group = new Group(item[1] + newline + item[2]);
				group = new Group(item[2]);
			//}
			//else{
				//group = new Group(item[2].substring(0, 30) + "...");
			//}
			
			// Add Expanded Details Text
			group.children.add(item[2]);
			
			// Add Group to Main Group
			groups.append(count, group);
			
			// Increase counter
			count++;
		}
	}
	
	// Build the Quizzes Array
	private void loadAgenciesArray()
	{
		// Set the Questions from the Data Array to the Questions Array
		for (String[] item : alListingsData)
		{
			alListings.add(item[1]);
		}
	}
	
	// Build the Quizzes Array
	private void loadLawsArray()
	{
		// Set the Questions from the Data Array to the Questions Array
		for (String[] item : alListingsData)
		{
			alListings.add(item[2] + " - " + item[1]);
		}
	}
	
	// Activation is successful. Add the Access code to the DB and activate the App
	final public void activateApp(String accessCode)
	{
		
		// Database connection
		final DatabaseHandler db = new DatabaseHandler(this);
		final String access = accessCode;
		
		// Dialog to display Successful activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Success");
		alert.setMessage("App Activated to Personnel Version");

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				
				// Insert the Access Code into the DB and call onCreate
				if (db.updateUserAccessCode(access))
				{
					
					// Activate the App to Full Version
					bIsFullVersion = true;
					
					// Close the connection to the DB
					db.close();

					// Create Restart App Intent
					Intent restartApp = getBaseContext().getPackageManager()
							.getLaunchIntentForPackage(getBaseContext().getPackageName());
					
					// Add Flag and start Intent
					restartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(restartApp);
				}
				
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();

	}
	
	// Display a dialog showing the message for Failing to activate the App
	final public void displayFailedActivationMessage(String message)
	{
		
		// Dialog to display failed activation
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setTitle("Activation Failed");
		alert.setMessage(message);

		// Positive Button
		alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() 
		{
			public void onClick(DialogInterface dialog, int whichButton) 
			{
				dialog.dismiss();
			}
		});

		// Display Dialog
		alert.show();
	}
}
