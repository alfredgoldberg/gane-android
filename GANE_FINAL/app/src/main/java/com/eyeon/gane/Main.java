package com.eyeon.gane;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.JsonParser;

import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Main extends Application {


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(newBase);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {

        super.onCreate();

        String path = this.getDatabasePath("GANEManagerV2").getPath();
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("MM_dd_yyyy");
        String strDate = sdf.format(c.getTime());
        launchLogcat(Environment.getExternalStorageDirectory() + "/GANE_logfile_" + strDate + ".txt");

        // Check for Data Connection
        if (isNetworkAvailable()) {
            // Instantiate Webservice and Parser class
            Webservices webService = new Webservices(this);
            JsonParser parser = new JsonParser();

            // Get the Priority Numbers
            JSONObject oNumJsonObject = webService.getPriorityNumbers();

            if (oNumJsonObject == null) Log.e("Main application class", "oNumJsonObject is null");
            ArrayList<String[]> aNumbers = parser.getPriorityNumbersFromJson(oNumJsonObject);

            SharedPreferences prefs = getSharedPreferences(Main.class.getSimpleName(), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();

            if (aNumbers != null) {
                for (String[] numbers : aNumbers)
                    editor.putString(numbers[1], numbers[2]);
            } else Log.e("Main application class", "aNumbers is null");
            editor.commit();
        } else {
            Log.v("Network Connection", "No connection detected in Main Application");
        }
    }

    // Check for Network Data Connection
    private boolean isNetworkAvailable() {

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        boolean netStatus = false;

        if (activeNetworkInfo != null)
            if (activeNetworkInfo.isAvailable())
                if (activeNetworkInfo.isConnected())
                    netStatus = true;

        return netStatus;
    }

    public Process launchLogcat(String filename) {
        Process process = null;
        String cmd = "logcat -f " + filename + "\n";
        try {
            process = Runtime.getRuntime().exec(cmd);
        } catch (IOException e) {
            process = null;
        }
        return process;
    }
}