package com.eyeon.gane.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JsonParser {

    ArrayList<String[]> results = new ArrayList<String[]>();
    static JSONObject oJson = null;

    public void JSONParser() {
        oJson = new JSONObject();
    }

    public JSONObject convertStringToJson(String result) {

        // Convert the string to JSONObject
        try {
            // Turn the entire string into a single JSON Object
            if (result != null) {
                oJson = new JSONObject(result);
                Log.e("String to JSON", oJson.toString());
            }
        } catch (JSONException e) {
            Log.e("String to JSON Conversion Failed", "Error: " + e.toString());
        }

        return oJson;
    }

    // Get the result of the AddDeviceToken submission
    public String checkAddDeviceTokenSubmission(JSONObject data) {

        // Initialize results value
        String userId = "";

        // Parse the JSON Array for the specific Array object nested within
        try {
            // Get Success Attributes
            boolean success = data.getBoolean("success");

            if (success)
                userId = data.getString("user_id");
        } catch (JSONException e) {
            Log.e("checkAddDeviceTokenSubmission failed", "Error: " + e.toString());
        }

        // Return the result
        return userId;
    }

    // Parse the Results of postActivationCode
    public String[] checkPostActivationCodeSubmission(JSONObject data) {

        // Create Results Array
        String[] results = null;

        try {
            // Get authenticated value
            boolean isAuthenticated = data.getBoolean("authenticated");

            // Check if Successful
            if (!isAuthenticated) {
                // Get Fail Message
                String failMessage = data.getString("message");

                // Add values to Array
                results = new String[2];
                results[0] = "false";
                results[1] = failMessage;
            } else {
                // Add value to Array
                results = new String[1];
                results[0] = "true";
            }
        } catch (JSONException e) {
            Log.e("checkPostActivationCodeSubmission failed", "Error: " + e.toString());
        }

        // Return results
        return results;
    }

    public ArrayList<String[]> getPriorityNumbersFromJson(JSONObject data) {

        ArrayList<String[]> results = new ArrayList<String[]>();

        try {

            // Create Array from JSON Object
            JSONArray numbers = data.getJSONArray("numbers");

            if (numbers != null) {
                // Loop Through Array to get Phone Numbers
                for (int i = 0; i < numbers.length(); i++) {
                    // Create Numbers object
                    JSONObject oCategory = numbers.getJSONObject(i);

                    // Get Question Attributes
                    String id = oCategory.getString("id");
                    String name = oCategory.getString("name");
                    String phone = oCategory.getString("phone");

                    // Add Question to String[] and add String[] to ArrayList
                    String[] saCategory = {id, name, phone};
                    results.add(saCategory);
                }
            }
        } catch (JSONException e) {
            Log.e("getPriorityNumbersFromJson failed", "Error: " + e.toString());
        }

        return results;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getAgencyCategoriesFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray categories = data.getJSONArray("categories");

            // Loop Through Array to get Questions
            for (int i = 0; i < categories.length(); i++) {
                // Create Question object
                JSONObject oCategory = categories.getJSONObject(i);

                // Get Question Attributes
                String id = oCategory.getString("id");
                String name = oCategory.getString("name");

                // Add Question to String[] and add String[] to ArrayList
                String[] saCategory = {id, name};
                results.add(saCategory);
            }
        } catch (JSONException e) {
            Log.e("getAgencyCategoriesFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getAgenciesByCategoryFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray agencies = data.getJSONArray("agencies");

            // Loop Through Array to get Questions
            for (int i = 0; i < agencies.length(); i++) {
                // Create Question object
                JSONObject oAgency = agencies.getJSONObject(i);

                // Get Question Attributes
                String id = oAgency.getString("id");
                String name = oAgency.getString("name");

                // Add Question to String[] and add String[] to ArrayList
                String[] saAgency = {id, name};
                results.add(saAgency);
            }
        } catch (JSONException e) {
            Log.e("getAgenciesByCategoryFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }


    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String> getAgencyByIdFromJson(JSONObject data) {

        ArrayList<String> agencyResults = new ArrayList<String>();

        // Parse the JSON Array for the specific Array object nested within
        try {
            // Create Array from JSON Object
            JSONObject agencies = data.getJSONObject("agency");

            // Get Question Attributes
            String id = agencies.getString("id");
            String name = agencies.getString("name");
            String details = agencies.getString("details");
            String hours = agencies.getString("hours_of_operation");
            String phoneNumber = agencies.getString("phone");
            String url = agencies.getString("url");

            // Add information to Results array
            agencyResults.add(id);
            agencyResults.add(name);
            agencyResults.add(details);
            agencyResults.add(hours);
            agencyResults.add(phoneNumber);
            agencyResults.add(url);
        } catch (JSONException e) {
            Log.e("getAgencyByIdFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return agencyResults;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getLawCategoriesFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray categories = data.getJSONArray("categories");

            // Loop Through Array to get Questions
            for (int i = 0; i < categories.length(); i++) {
                // Create Question object
                JSONObject oCategory = categories.getJSONObject(i);

                // Get Question Attributes
                String id = oCategory.getString("id");
                String name = oCategory.getString("name");

                // Add Question to String[] and add String[] to ArrayList
                String[] saCategory = {id, name};
                results.add(saCategory);
            }
        } catch (JSONException e) {
            Log.e("getLawCategoriesFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getLawsByCategoryFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray laws = data.getJSONArray("laws");

            // Loop Through Array to get Questions
            for (int i = 0; i < laws.length(); i++) {
                // Create Question object
                JSONObject oLaw = laws.getJSONObject(i);

                // Get Question Attributes
                String id = oLaw.getString("id");
                String statuteId = oLaw.getString("statute_id");
                String description = oLaw.getString("description");

                // Add Question to String[] and add String[] to ArrayList
                String[] saLaws = {id, statuteId, description};
                results.add(saLaws);
            }
        } catch (JSONException e) {
            Log.e("getLawsByCategoryFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getQuizzesFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray quizzes = data.getJSONArray("quizzes");

            // Loop Through Array to get Questions
            for (int i = 0; i < quizzes.length(); i++) {
                // Create Question object
                JSONObject oQuizzes = quizzes.getJSONObject(i);

                // Get Question Attributes
                String id = oQuizzes.getString("id");
                String type = oQuizzes.getString("quiz_type");
                String name = oQuizzes.getString("name");
                boolean bGetClientInfo = oQuizzes.getBoolean("get_client_info");

                String sGetClientInfo = "";
                if (bGetClientInfo)
                    sGetClientInfo = "true";
                else
                    sGetClientInfo = "false";

                // Add Question to String[] and add String[] to ArrayList
                String[] saQizzes = {id, type, name, sGetClientInfo};
                results.add(saQizzes);
            }
        } catch (JSONException e) {
            Log.e("getQuizzesFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Get the Client ID
    public int getAddClientResultsFromJson(JSONObject data) {

        // Create Default Value
        int results = 0;

        // If Successful, grab the Client ID
        try {
            if (data.getBoolean("success"))
                results = data.getInt("client_id");
        } catch (JSONException e) {
            Log.e("getAddClientResultsFromJson failed", "Error: " + e.toString());
        }

        // Return the Result
        return results;
    }

    public int getMakeReferralResultsFromJson(JSONObject data) {
        int results = 0;

        // If Successful, grab the Referral ID
        try {
            if (data.getBoolean("success"))
                results = data.getInt("referral_id");
        } catch (JSONException e) {
            Log.e("getMakeReferralResultsFromJson failed", "Error: " + e.toString());
        }

        return results;
    }

    // Confirm Referral
    public boolean getReferClientResultsFromJson(JSONObject data) {

        // Create Default Value
        boolean results = false;

        // If Successful, grab the Referral ID
        try {
            if (data.getBoolean("success"))
                results = true;
        } catch (JSONException e) {
            Log.e("getReferClientResultsFromJson failed", "Error: " + e.toString());
        }

        // Return the Result
        return results;
    }

    // Get an ArrayList of the Questions from the JSON Array
    public ArrayList<String[]> getQuestionsFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Get the Session NVP
            String[] sessionInfo = {"session_id", data.getString("session_id")};

            // Get Slideshow Questions Flag
            boolean slideshow = data.getBoolean("is_slide_show");
            String slideshow_text = "";
            if (slideshow)
                slideshow_text = "true";
            else
                slideshow_text = "false";

            String[] slideshowQuestionsFlag = {"is_slide_show", slideshow_text};

            String section_name = data.getString("section_name");
            String[] SectionName = {"section_name", section_name};

            String score_metric = data.getString("score_metric");
            String[] ScoreMetric = {"score_metric", score_metric};

            // Add Value Pairs to ArrayList
            results.add(sessionInfo);
            results.add(slideshowQuestionsFlag);
            results.add(SectionName);
            results.add(ScoreMetric);

            // Create Array from JSON Object
            JSONArray questions = data.getJSONArray("questions");

            // Loop Through Array to get Questions
            for (int i = 0; i < questions.length(); i++) {
                // Create Question object
                JSONObject oQuestion = questions.getJSONObject(i);

                // Get Question Attributes
                String id = oQuestion.getString("id");
                String question = oQuestion.getString("question");
                String point_value = oQuestion.getString("point_value");

                String title = "";
                String answer = "";
                String time_to_answer = "";
                String pos_button_desc = "";
                String neg_button_desc = "";
                String pos_button_label = "";
                String neg_button_label = "";

                String[] saQuestion;

                if (slideshow) {
                    title = oQuestion.getString("title");
                    answer = oQuestion.getString("answer");
                    time_to_answer = oQuestion.getString("time_to_answer");
                    pos_button_desc = oQuestion.getString("positive_button_description");
                    neg_button_desc = oQuestion.getString("negative_button_description");
                    pos_button_label = oQuestion.getString("positive_button_label");
                    neg_button_label = oQuestion.getString("negative_button_label");

                    // Add Question to String[] and add String[] to ArrayList
                    saQuestion = new String[]{id, title, question, answer, time_to_answer, pos_button_desc, neg_button_desc,
                            pos_button_label, neg_button_label, point_value};
                } else {
                    saQuestion = new String[]{id, question, point_value};
                }

                results.add(saQuestion);
            }
        } catch (JSONException e) {
            Log.e("getQuestionsFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Get the results from the Quiz submission from the JSON Array
    public ArrayList<String> getQuizResultsFromJson(JSONObject data) {

        ArrayList<String> quizResults = new ArrayList<String>();

        // Create Default Value
        String sPassQuiz = "";
        String sIsSelfNeglect = "";

        // Try to parse the JSONObject for the Pass/Fail Flag
        try {

            if (!data.isNull("pass_quiz")) {
                // Get the Results
                boolean pass_quiz = data.getBoolean("pass_quiz");
                boolean is_self_neglect = data.getBoolean("is_self_neglect");

                if (pass_quiz)
                    sPassQuiz = "pass";
                else
                    sPassQuiz = "fail";

                if (is_self_neglect)
                    sIsSelfNeglect = "yes";
                else
                    sIsSelfNeglect = "no";

                // Add them to the Array
                quizResults.add(sPassQuiz);
                quizResults.add(sIsSelfNeglect);
            } else if (!data.isNull("next_section")) {
                // Add them to the Array
                quizResults.add("Next Section");
            }
        } catch (JSONException e) {
            Log.e("getQuizResultsFromJson failed", "Error: " + e.toString());
        }

        // Return the Result
        return quizResults;
    }

    // Get the results of the Matties Call Activation JSON Array
    public boolean getMattiesCallSubmissionResultsFromJson(JSONObject data) {
        // Create Default Value
        boolean results = false;

        // Try to parse the JSONObject for the Pass/Fail Flag
        try {
            results = data.getBoolean("success");
        } catch (JSONException e) {
            Log.e("getMattiesCallSubmissionResultsFromJson failed", "Error: " + e.toString());
        }

        // Return the Result
        return results;
    }

    // Get an ArrayList of Unread Notifications from the JSON Array
    public ArrayList<String[]> getUnreadNotificationsFromJson(JSONObject data) {

        results = new ArrayList<String[]>();

        // Parse the JSON Array for the specific Array object nested within
        try {

            // Create Array from JSON Object
            JSONArray notifications = data.getJSONArray("notifications");

            // Check for Notifications
            if (data.getBoolean("has_notifications")) {

                // Loop Through Array to get Notifications
                for (int i = 0; i < notifications.length(); i++) {
                    // Create Notification object
                    JSONObject oNotification = notifications.getJSONObject(i);

                    // Get Notification Attributes
                    String id = oNotification.getString("id");
                    String message = oNotification.getString("message");

                    // Add Notification to String[] and add String[] to ArrayList
                    String[] saNotification = {id, message};
                    results.add(saNotification);
                }
            }
        } catch (JSONException e) {
            Log.e("getUnreadNotificationsFromJson failed", "Error: " + e.toString());
        }

        // Return the ArrayList
        return results;
    }

    // Check the success of markNotificationRead
    public boolean checkMarkNotificationReadFromJson(JSONObject data) {
        // Create Default Value
        boolean results = false;

        // Try to parse the JSONObject for the Pass/Fail Flag
        try {
            results = data.getBoolean("success");
        } catch (JSONException e) {
            Log.e("checkMarkNotificationReadFromJson failed", "Error: " + e.toString());
        }

        // Return the Result
        return results;
    }
}
