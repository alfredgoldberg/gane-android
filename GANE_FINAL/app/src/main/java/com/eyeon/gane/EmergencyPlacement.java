package com.eyeon.gane;

import java.util.ArrayList;

import org.json.JSONException;
import org.json.JSONObject;

import com.eyeon.gane.services.Webservices;
import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.StateSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class EmergencyPlacement extends AppCompatActivity
{

	// Activity Variables
	Bundle savedData = new Bundle();
	Context mContext;
	
	// Layout Widgets
	Button bContactTERF;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// Save State and call super
		savedData = savedInstanceState;
		super.onCreate(savedData);
		
		// Get Activity
		mContext = this;
		
		// Set the Layout and Title
		setContentView(R.layout.emergency_placement_2);
		setTitle("TERF");
		
		// Get the Button NoTouch/Touch Graphics
		Drawable buttonTouch = getResources().getDrawable(R.drawable.button_touch);
		Drawable buttonNoTouch = getResources().getDrawable(R.drawable.button_notouch);
		
		TextView tvCrisisLine = (TextView)findViewById(R.id.tvCrisisParagraph);
		tvCrisisLine.setMovementMethod(LinkMovementMethod.getInstance());
		
		// Create the clickable Effects
		StateListDrawable buttonEffects = new StateListDrawable();
		buttonEffects.addState(new int[] {android.R.attr.state_pressed}, buttonTouch);
		buttonEffects.addState(StateSet.WILD_CARD, buttonNoTouch);
		
		bContactTERF = (Button)findViewById(R.id.btnCallTERF);
		bContactTERF.setBackground(buttonEffects);
		bContactTERF.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v) 
			{

				Webservices webservices = new Webservices(getApplicationContext());
				DatabaseHandler db = new DatabaseHandler(mContext);
				String sDeviceID = db.getRegId();
				//Moved from placement
				try {
					JSONObject jsonObject =	webservices.postTerf(sDeviceID);
					Log.e("JSON TERF",jsonObject.toString());
				} catch (JSONException e) {
					e.printStackTrace();
				}

				// Create Dialog
				AlertDialog.Builder TERFCall = new AlertDialog.Builder(mContext);

				// Set Title and Message
				TERFCall.setTitle("TERF Administration");
				TERFCall.setMessage("Do you want to call the TERF Administrator?");
				
				// Set positive button
				TERFCall.setPositiveButton("OK", new DialogInterface.OnClickListener() 
				{
					public void onClick(DialogInterface dialog, int whichButton) 
					{
						// Phone Number
						String phoneNumber = null;
						Webservices callPriorityNumbers = new Webservices(mContext);
						JsonParser parser = new JsonParser();	
						
						// Call the webservice to get the TERF's phone number
						JSONObject results = callPriorityNumbers.getPriorityNumbers();
						
						// Parse the Data and return an ArrayList of Phone Numbers
						ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
						alPhoneNumbers = parser.getPriorityNumbersFromJson(results);
						
						// Loop through the phone numbers
						for (String[] numbers : alPhoneNumbers)
						{
							// Search for TERF's Call Contact Information
							if (numbers[1].contains("TERF"))
							{
								phoneNumber = numbers[2];
							}
						}
						Webservices webservices = new Webservices(mContext);
						DatabaseHandler db = new DatabaseHandler(mContext);
						String sDeviceID = db.getDeviceID();

						MyPhoneListener phoneListener = new MyPhoneListener(getApplicationContext(),sDeviceID);
						TelephonyManager telephonyManager =
								(TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
						// receive notifications of telephony state changes
						telephonyManager.listen(phoneListener,PhoneStateListener.LISTEN_CALL_STATE);
						// Create Intent to call phone number
						Intent phoneCall = new Intent(Intent.ACTION_CALL);
						phoneCall.setData(Uri.parse("tel:" + Uri.encode(phoneNumber)));
						phoneCall.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(phoneCall);
					}
				});

				// Set negative button
				TERFCall.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
				{
					  public void onClick(DialogInterface dialog, int whichButton) 
					  {
						  dialog.dismiss();
					  }
				});

				// Show Dialog
				TERFCall.show();
			}
		});
	}
	
	@Override
	protected void onStart()
	{
		super.onStart();
	}
	
	@Override
	protected void onResume()
	{
		
		if (savedData != null)
			onRestoreInstanceState(savedData);

		super.onResume();
	}
	
	@Override
	protected void onPause()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);
		
		super.onPause();
	}
	
	@Override
	protected void onStop()
	{
		if (savedData != null)
			onSaveInstanceState(savedData);

		super.onStop();
	}
	
	@Override
	protected void onDestroy()
	{
		super.onDestroy();
		
		// Destroy the Activity
		finish();
	}

	@Override
	public void onSaveInstanceState(Bundle savedInstanceState)
	{
		// Call Super Class
		super.onSaveInstanceState(savedInstanceState);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		// Call Super Class
		super.onRestoreInstanceState(savedInstanceState);
	}

	private class MyPhoneListener extends PhoneStateListener {

		private boolean onCall = false;
		private Context ws;
		private String id;

		public MyPhoneListener(Context context, String appId){
			this.ws = context;
			this.id = appId;
		}

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {

			switch (state) {

				case TelephonyManager.CALL_STATE_OFFHOOK:
					// one call exists that is dialing, active, or on hold
					try {
						Webservices webservices = new Webservices(getApplicationContext());
						JSONObject json=	webservices.postContactUs(id+"_phone_ringing");
						Log.e("Json_phone",json.toString());
					} catch (JSONException e) {
						e.printStackTrace();
					}
					Log.e("Phone","active/hold");
					//because user answers the incoming call
					onCall = true;
					break;

				case TelephonyManager.CALL_STATE_IDLE:
					// in initialization of the class and at the end of phone call

					// detect flag from CALL_STATE_OFFHOOK
					if (onCall == true) {

						Log.e("Phone","restart");
						// restart our application
						Intent restart = getBaseContext().getPackageManager().
								getLaunchIntentForPackage(getBaseContext().getPackageName());
						restart.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(restart);

						onCall = false;
					}
					break;
				default:
					break;
			}

		}
	}

}
