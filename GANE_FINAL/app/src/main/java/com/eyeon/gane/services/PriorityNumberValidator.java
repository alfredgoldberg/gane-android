package com.eyeon.gane.services;

import java.util.ArrayList;

import org.json.JSONObject;

import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class PriorityNumberValidator extends Service
{

	@Override
	public void onCreate()
	{
		super.onCreate();
	}
	
	@Override
	public void onStart(Intent intent, int startId)
	{
		
		Log.v("PriorityNumberValidator", "The service has started");
		
		// Get the Phone Numbers from the webservice call here and load the array
		Webservices PhoneNumbers = new Webservices(this);
		
		// Get POST Results
		JSONObject results = PhoneNumbers.getPriorityNumbers();
		
		// Parse the Data and return an ArrayList of Phone Numbers
		JsonParser parser = new JsonParser();
		ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
		alPhoneNumbers = parser.getPriorityNumbersFromJson(results);
		
		// Make Connection to the DB
		DatabaseHandler db = new DatabaseHandler(this);
		
		// Loop throug the Array
		for (String[] numbers : alPhoneNumbers)
		{
			// Check to see if the Record already exists
			if (!db.checkIfPriorityNumberExistsByName(numbers[1]))
			{
				// Add the Number to the DB
				db.addPriorityNumber(numbers);
			}
			else
			{
				// Update the Number in the DB
				db.updatePhoneNumber(numbers);
			}
		}
		
		// Close the Database
		db.close();
		
		// Temporary Validation
		Log.v("Priority Numbers Update", "Update Successful");

		// Destroy the Activity once finished
		onDestroy();
	}
	

	@Override
	public IBinder onBind(Intent intent) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void onDestroy()
	{

		try 
		{
			Log.v("PriorityNumberValidator", "The service has stopped");
			finalize();
		} 
		catch (Throwable e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.onDestroy();
	}
}
