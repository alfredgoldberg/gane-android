package com.eyeon.gane.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

public class MyLocationListener implements LocationListener 
{

	Context myContext;
	
	boolean bIsGPSEnabled = false;
	boolean bIsNetworkEnabled = false;
	boolean bCanGetLocation = false;
	
	protected LocationManager oLocationManager;
	Location oLocation;
	
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;
	
	Double dLongitude;
	Double dLatitude;
	
	public MyLocationListener(Context context)
	{
		myContext = context;
		getLocation();
	}
	
	@SuppressWarnings("static-access")
	public Location getLocation()
	{
		
		try 
		{
			oLocationManager = (LocationManager) myContext.getSystemService(myContext.LOCATION_SERVICE);
 
            // getting GPS status
			bIsGPSEnabled = oLocationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
 
            // getting network status
			bIsNetworkEnabled = oLocationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
 
            if (!bIsGPSEnabled && !bIsNetworkEnabled) 
            {
                // no network provider is enabled
            } 
            else 
            {
                this.bCanGetLocation = true;
                // First get location from Network Provider
                if (bIsNetworkEnabled) 
                {
                	oLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                    Log.d("Network", "Network");
                    if (oLocationManager != null) 
                    {
                    	oLocation = oLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (oLocation != null) 
                        {
                            dLatitude = oLocation.getLatitude();
                            dLongitude = oLocation.getLongitude();
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (bIsGPSEnabled) 
                {
                    if (oLocation == null) 
                    {
                    	oLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
                        Log.d("GPS Enabled", "GPS Enabled");
                        if (oLocationManager != null) 
                        {
                        	oLocation = oLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (oLocation != null) 
                            {
                                dLatitude = oLocation.getLatitude();
                                dLongitude = oLocation.getLongitude();
                            }
                        }
                    }
                }
            }
 
        } 
		catch (Exception e) 
        {
            e.printStackTrace();
        }
 
        return oLocation;
	}
	
	public boolean canGetLocation()
	{
		return bCanGetLocation;
	}
	
	public void stopUsingGPS()
	{
		if(oLocationManager != null)
			oLocationManager.removeUpdates(MyLocationListener.this);
	}
	
	// Get the Longitude
	public Double getLongitude()
	{
		
		if (oLocation != null)
			dLongitude = oLocation.getLongitude();
		
		return dLongitude;
	}
	
	// Get the Latitude
	public Double getLatitude()
	{
		
		if (oLocation != null)
			dLatitude = oLocation.getLatitude();
		
		return dLatitude;
	}
	
	public void showSettingsAlert()
	{
		
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(myContext);
      
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");
  
        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");
  
        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() 
        {
            public void onClick(DialogInterface dialog,int which) 
            {
                Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                myContext.startActivity(intent);
            }
        });
  
        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() 
        {
            public void onClick(DialogInterface dialog, int which) 
            {
            	dialog.cancel();
            }
        });
  
        // Showing Alert Message
        alertDialog.show();
    }

	@Override
	public void onLocationChanged(Location location) 
	{

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) 
	{

	}

	@Override
	public void onProviderEnabled(String provider) 
	{

	}

	@Override
	public void onProviderDisabled(String provider) 
	{

	}
}
