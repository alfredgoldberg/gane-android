package com.eyeon.gane;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

public class URLWebViewActivity extends AppCompatActivity
{
	
	// Activity Variables
	private WebView webView;
	private String url;
	private boolean TERF = false;
	private boolean hasLoaded = false;
	
	public void onCreate(Bundle savedInstanceState)
	{
		// Call super and Set Content View
		super.onCreate(savedInstanceState);
		setContentView(R.layout.url_web_view);
		
		// Get Intent and its extras
		Intent intent = getIntent();
		url = intent.getStringExtra("url");
		TERF = intent.getBooleanExtra("is_terf", false);
		
		// Get Webview Widget and load URL
		webView = (WebView)findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl(url);
	}
	
	@Override
	public void onStart()
	{
		super.onStart();
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStarted(this);
	}
	
	@Override
	public void onResume()
	{
		super.onResume();
		
		// To capture the return of WebViews that were not contained in App
		if (hasLoaded)
		{
			
			// Check if returning from Map2Care
			if(TERF)
			{
				
				// Activity Return with Result
				Intent data = new Intent();
			    setResult(RESULT_OK, data);
				finish();
			}
			else
				onBackPressed();
		}
		
		// Update Loaded Variable
		hasLoaded = true;
	}
	
	@Override
	public void onStop()
	{
		
		// UrbanAirship
		//if (UAirship.shared().isFlying())
		//	UAirship.shared().getAnalytics().activityStopped(this);
		
		super.onStop();
	}
}
