package com.eyeon.gane.utils;

/**
 * Created by dorul on 8/12/2016.
 */

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.eyeon.gane.R;
import com.eyeon.gane.services.Webservices;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import org.json.JSONObject;

public class GCMRegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    SharedPreferences sharedPreferences;

    public GCMRegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        Intent registrationComplete = new Intent(GCMSharedPrefs.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("prefix", "");
        String token = sharedPreferences.getString(GCMSharedPrefs.REG_ID, "");

        try {
            InstanceID instanceID = InstanceID.getInstance(this);

            if (!intent.getExtras().getBoolean("register")) {
                instanceID.deleteToken(getString(R.string.gcm_defaultSenderId), GoogleCloudMessaging.INSTANCE_ID_SCOPE);
                sharedPreferences.edit().clear().apply();

            } else {
                if (!intent.getExtras().getBoolean("tokenRefreshed") && !token.equals("")) {
                    registrationComplete.putExtra("prefix", "Old Token:\n");
                } else {
                    token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                    registrationComplete.putExtra("prefix", "Fresh Token:\n");
                }
                Log.i(TAG, "GCM Registration Token: " + token);

                sharedPreferences.edit().putString(GCMSharedPrefs.REG_ID, token).apply();
                sharedPreferences.edit().putBoolean(GCMSharedPrefs.REG_SUCCESS, true).apply();

                //registerWithServer(token, instanceID);
                Webservices ws = new Webservices(this);


                String userIdFromPrefs = "";
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                userIdFromPrefs = preferences.getString("user_id_first_open", "");


                    DatabaseHandler db = new DatabaseHandler(this);

                    boolean exists = false;

                    if (exists) {
                        Log.v("user code", "user already in database");
                    } else {
                        // db.addUser(userId, "", token);
                        db.updateUserRegId(token);
                        Webservices web = new Webservices(this);
                        JsonParser parser = new JsonParser();
                        String deviceID = db.getDeviceID();
                        db.close();

                        // Send Data to server and check it
                        JSONObject data = web.addDeviceToken(token, deviceID);
                        String userID = parser.checkAddDeviceTokenSubmission(data);

                        // Update UserID if not present
                        if (deviceID.length() == 0) {
                            db = new DatabaseHandler(this);
                            db.updateDeviceId(userID);
                            db.close();
                            Log.v("Device Registration", "New UserID Created");
                        } else {
                            Log.v("Device Update", "UserID Already present");
                        }

                        DatabaseHandler dbAppVersion = new DatabaseHandler(this);
                        int storedAppVersion = dbAppVersion.getAppVersion();
                        int currentAppVersion = getAppVersion(this);

                        // Update App Version if different
                        if (storedAppVersion != currentAppVersion) {
                            Log.v("Device Update", "App version updated.");
                            dbAppVersion.updateAppVersion(currentAppVersion);
                        }

                        // Close the DB
                        dbAppVersion.close();
                    }
                }
        } catch (Exception e) {
            sharedPreferences.edit().putBoolean(GCMSharedPrefs.REG_SUCCESS, false).apply();
            Log.d(TAG, "Registration service failed.", e);
        }

        if (intent.getExtras().getBoolean("register"))
            registrationComplete.putExtra("register", true);
        else
            registrationComplete.putExtra("register", false);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
}
