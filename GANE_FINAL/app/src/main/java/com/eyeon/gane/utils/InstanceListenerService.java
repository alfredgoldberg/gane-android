package com.eyeon.gane.utils;

/**
 * Created by dorul on 8/12/2016.
 */
import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

public class InstanceListenerService extends InstanceIDListenerService {

    private static final String TAG = "InstanceIDLS";

    @Override
    public void onTokenRefresh() {
        Intent intent = new Intent(this, GCMRegistrationIntentService.class);
        intent.putExtra("register",true);
        intent.putExtra("tokenRefreshed",true);
        startService(intent);
    }
}