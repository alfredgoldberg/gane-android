package com.eyeon.gane.services;

import java.util.ArrayList;

import org.json.JSONObject;

import com.eyeon.gane.utils.DatabaseHandler;
import com.eyeon.gane.utils.JsonParser;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyBroadcastReceiver extends BroadcastReceiver
{

	// Milliseconds * Seconds * Minutes * Hours = 24 Hours
	final static int alarmDelay = 1000 * 60 * 60 * 24;
	
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		
		// Checks for Device boot then Calls the Priority Number Validator Service on device boot
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) 
		{
			
			Log.v("MyBroadcastReciever", "Device boot has been captured");
			
			// Update Priority Numbers
			// Get the Phone Numbers from the webservice call here and load the array
			Webservices PhoneNumbers = new Webservices(context);
			
			// Get POST Results
			JSONObject results = PhoneNumbers.getPriorityNumbers();
			
			// Parse the Data and return an ArrayList of Phone Numbers
			JsonParser parser = new JsonParser();
			ArrayList<String[]> alPhoneNumbers = new ArrayList<String[]>();
			alPhoneNumbers = parser.getPriorityNumbersFromJson(results);
			
			// Make Connection to the DB
			DatabaseHandler db = new DatabaseHandler(context);
			
			// Loop through the Array
			for (String[] numbers : alPhoneNumbers)
			{
				// Check to see if the Record already exists
				if (!db.checkIfPriorityNumberExistsByName(numbers[1]))
				{
					// Add the Number to the DB
					db.addPriorityNumber(numbers);
				}
				else
				{
					// Update the Number in the DB
					db.updatePhoneNumber(numbers);
				}
			}
			
			// Setup Alarm to repeat checking the Priority Numbers
	        setAlarm(context);
			Log.v("MyBroadcastReceiver", "Alarm has been set");
	    }

	}
	
	// Create Alarm
	public void setAlarm(Context context)
	{
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, PriorityNumberValidator.class);
        PendingIntent pi = PendingIntent.getService(context, 0, i, 0);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), alarmDelay, pi);
        Log.v("Set Priority Number Updater Alarm", "PriorityNumberValidator Alarm is set");
	}
}
